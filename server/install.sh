#!/bin/sh
####################################
#
# Install the DPU Web Server service.
#
####################################

SERVER_SRC="src"
SERVER_DEST="/srv/dpu"

CONFIG_FILENAME="dpuserver.conf"
CONFIG_SRC=$SERVER_SRC"/"$CONFIG_FILENAME
CONFIG_DEST="/etc/init/"$CONFIG_FILENAME

DEBUG_BIN="/usr/local/bin/dpu"

# Create a directory to run the dpuserver.
mkdir -m 755 $SERVER_DEST

# Copy server files.
cp -R $SERVER_SRC"/*" $SERVER_DEST
chmod 755 -R $SERVER_DEST

# Copy dpuserver service config file.
cp $CONFIG_SRC $CONFIG_DEST
chmod 755 $CONFIG_DEST

# change directory to server files and update/install/build dependencies
cd $SERVER_DEST

npm update
npm install -g node-gyp


node-gyp build

# install utility program

g++ dpu.cc dpuIO.cc -o $DEBUG_BIN
export dpu $DEBUG_BIN



#make install command line tool "dpudev -setVoxel 2 4 4" <- this should wrap the functionality of dpu_interface.h,
#			ie. "setVoxel 2 3 5", "clearVoxel 0 0 3", "setData 22 0xff"
# this way you can test the dpu functionality without having to call node

# Restart upstart to register the dpuserver service.
initctl reload-configuration