



#include "dpuIO.h"

int gpioLookup[] =
{
  55, // PORTA, Pin 0 (J6, Pin 23, gpio55) 0   DATA_0
  35, // PORTA, Pin 1 (J6, Pin 24, gpio35) 1   DATA_1
  54, // PORTA, Pin 2 (J6, Pin 21, gpio54) 2   DATA_2
  34, // PORTA, Pin 3 (J6, Pin 22, gpio34) 3   DATA_3
  61, // PORTA, Pin 4 (J6, Pin 19, gpio61) 4   DATA_4
  33, // PORTA, Pin 5 (J6, Pin 20, gpio33) 5   DATA_5
  36, // PORTA, Pin 6 (J6, Pin 17, gpio36) 6   DATA_6
  32, // PORTA, Pin 7 (J6, Pin 18, gpio32) 7   DATA_7

  56, // PORTB, Pin 0 (J6, Pin 26, gpio56) 8   OP_0
  50, // PORTB, Pin 1 (J6, Pin 25, gpio50) 9   OP_1
  59, // PORTB, Pin 2 (J6, Pin 28, gpio59) 10  OP_TRIGGER
  51  // PORTB, Pin 3 (J6, Pin 27, gpio51) 11  OP_3
};


int pinStreams[12];




// --- filesystem operations ---

void writePin(int pinIndex, bool pinValue)
{
    write( pinStreams[pinIndex], (pinValue? "1" : "0"), 1 );
}

bool readPin(int pinIndex)
{
    char inputValue[1];
    read( pinStreams[pinIndex], inputValue, 1 );
    return strncmp( inputValue, "1", 1 );
}

// ----------------




// --- pin operations ---

void setTrigger(bool value)
{
	writePin( OP_TRIGGER, value );
}

void setOp(char opcode)
{
	writePin( OP_0, (opcode & 0x01) );
	writePin( OP_1, (opcode & 0x02) );
	writePin( OP_2, (opcode & 0x04) );
}

void setBus(char data)
{
	writePin( DATA_0, (data & 0x01) );
	writePin( DATA_1, (data & 0x02) );
	writePin( DATA_2, (data & 0x04) );
	writePin( DATA_3, (data & 0x08) );
	writePin( DATA_4, (data & 0x10) );
	writePin( DATA_5, (data & 0x20) );
	writePin( DATA_6, (data & 0x40) );
	writePin( DATA_7, (data & 0x80) );
}

// ----------------


// --- dpu operations ---

void write(char value)
{
	callOperation( OP_DEFINE, value );
}

void writeAndIncrement(char value)
{
	callOperation( OP_DEFINC, value );
}

void setMask(char value)
{
	callOperation( OP_SETMSK, value );
}

void clearMask(char value)
{
	callOperation( OP_CLRMSK, value );
}

void setAddress(char value)
{
	callOperation( OP_SETADR, value );
}

void setLayerCount(char value)
{
	callOperation( OP_SETLRC, value );
}

void setRegisterCount(char value)
{
	callOperation( OP_SETRGC, value );
}

// ----------------


// --- Draw operations ---

void setVoxel(int x, int y, int z, bool a)
{
	setAddress( (y<<3) + z );
	
	if(a)
	{
		setMask( 0x01<<x );
	}
	else
	{
		clearMask( 0x01<<x );
	}
}


// ----------------


// --- IO operations ---

void callOperation(char opcode, char value)
{
	setTrigger( true );		// Reset the trigger - "ready to go"
	setOp( opcode );		// Set the current operation
	setBus( value );		// Set the value on the data bus
	setTrigger( false );	// Call the trigger - "perform operation"
}


int openDpuIO(void)
{
    int export_fd;
    int direction_fd;
    char buffer[128];

    // open the gpio export file
    export_fd = open( "/sys/class/gpio/export", O_WRONLY | O_NDELAY, 0 );

    // if the export file did not open correctly, fail
    if ( export_fd == 0 )
    {
        return 1;//scope.Close(String::New("Error opening export path: /sys/class/gpio/export.\n"));
    }

    // loop through all pin values
    for( int i = 0; i < 12; i++ )
    {
        int pinNum = gpioLookup[i];

        // convert pin int to pin string
        sprintf( buffer, "%d", pinNum );

        // write the string to the opened export file
        write( export_fd, buffer, strlen(buffer) );


        // format a direction stream path from pin number
        sprintf( buffer, "/sys/class/gpio/gpio%d/direction", pinNum );
        direction_fd = open( buffer, O_WRONLY | O_NDELAY, 0 );

        // if the direction stream path did not open successfully
        if ( direction_fd == 0 )
        {
            return 2;//scope.Close(String::New("Error opening direction stream.\n"));
        }

        // Set the direction of the GPIO ("out")
		write( direction_fd, "out", 3 );

        // close the direction stream
        close( direction_fd );


        // format a value stream path from pin number
        sprintf( buffer, "/sys/class/gpio/gpio%d/value", pinNum );
        pinStreams[i] = open( buffer, O_WRONLY | O_NDELAY, 0 );

        // if the value stream path did not open successfully
        if ( pinStreams[i] == 0 )
        {
            return 3;//scope.Close(String::New("Error opening value stream.\n"));
        }
    }

    // close out the export file, we are done with it
    close( export_fd );

	// call a "no-op" to prime all pin values (zero the data bus, zero the opcode, ready the op trigger)
    callOperation( OP_NULL, 0 );

    return 0;
}

int closeDpuIO(void)
{
    for( int i = 0; i < 12; i++ )
    {
        close( pinStreams[i] );
    }

    return 0;
}


// ----------------
