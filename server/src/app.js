
/**
 * Module dependencies.
 */

// dpu object
var dpu = {};

// dpu.enabled = true;
// dpu.active = false;
// dpu.nucleus = buildNucleus(true);
dpu.buffer = [];
for(var i = 0; i < 64; i++){
	dpu.buffer[i] = 0xff;
}

dpu.device = require('./build/Release/dpuInterface');

// server vars
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');


var app = express();

// all environments
app.set('port', process.env.PORT || 4000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);


var httpServer = http.createServer(app);
httpServer.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


var io = require('socket.io').listen(httpServer);

io.sockets.on('connection', function (socket) {
    

	socket.on('requestCurrentBuffer', function(){
		io.sockets.emit('respondCurrentBuffer', dpu.buffer);
	});
	
  socket.on('draw', function (bufferData)
  {
  	draw(bufferData);
  });
	
});


console.log("Initializing dpu server");
console.log( dpu.device.setup() );


console.log( dpu.device.drawFrame(dpu.buffer) );

function draw(bufferData)
{

		dpu.buffer = bufferData;
		dpu.device.drawFrame(bufferData);
}

var aObj = planeSwipe();

// setInterval(function(){
	
	
	// aObj.update();
	// dpu.device.draw(dpu.buffer);
		
// }, 20);

function planeSwipe()
{
	var aniObj = {};
	
	aniObj.framerate = 60;
	aniObj.framecount = 0;
	aniObj.delta = 0;
	aniObj.buffer = dpu.buffer;
	
	
	var x = 0;
	var y = 0;
	var z = 0;
	
	aniObj.update = function(){
		
		//if(++aniObj.delta < 25) { return; }
		//aniObj.delta = 0;
		
		clear(dpu.buffer);
		
		setVoxel(x,y,z,true, dpu.buffer);
		setVoxel((7-x),(7-y),(7-z),true, aniObj.buffer);
		
		if(++x > 7)
		{
			if(++y > 7)
			{
				if(++z > 7)
				{
					z = 0;
				}
				y = 0;
			}
			x = 0;
		}
		
	};
	
	
	return aniObj;
}

function clear(bufferData){
	
		for(var i = 0; i < 64; i++){
			bufferData[i] = 0;
		}
	}


function setVoxel(x,y,z,a,bufferData){	
		
		//if(bufferData == null){ bufferData = buffer; }
		
		x <<= 0;
		y <<= 0;
		z <<= 0;
		
		var i = toAddress(y,z);
		if(a)   { bufferData[i] |= (0x01<<x); }
		else    { bufferData[i] &= ~(0x01<<x); }
	}
	
	
	function toAddress(y,z){
		return (y<<3) + z;
	}
