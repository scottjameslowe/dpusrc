#ifndef DPU_IO_H
#define DPU_IO_H

/*

  * GPIO PIN CHART *
  J6 Pin - Pin Name

  na  - 1 - VBUS out from USB Host Port #3
  na  - 2 - VBUS out from USB Host Port #4
  na  - 3 - USB Host Port #3 Data Minus
  na  - 4 - USB Host Port #4 Data Minus
  na  - 5 - USB Host Port #3 Data Plus
  na  - 6 - USB Host Port #4 Data Plus
  na  - 7 - Digital Ground
  na  - 8 - Digital Ground
  0   - 9	- GPIO_38 (GPMC Address/Data Bit 14)
  1   - 10	- GPIO_37 (GPMC Address/Data Bit 13)
  na  - 11 - Warm Reset
  na  - 12 - Power on input to TWL6030 (ref. to VBAT)
  na  - 13 - Hands Free Left Speaker Out (+)
  na  - 14 - Hands Free Right Speaker Out (+)
  na  - 15 - Hands Free Left Speaker Out (-)
  na  - 16 - Hands Free Right Speaker Out (-)
  2   - 17	- GPIO_36 (GPMC Address/Data Bit 13)
  3   - 18	- GPIO_32 (GPMC Address/Data Bit 8)
  4   - 19	- GPIO_61 (GPMC Wait input 0)
  5   - 20	- GPIO_33 (GPMC Address/Data Bit 9)
  6   - 21	- GPIO_54 (GPMC Write Protect)
  7   - 22	- GPIO_34 (GPMC Address/Data Bit 10)
  8   - 23	- GPIO_55 (GPMC Clock Out)
  9   - 24	- GPIO_35 (GPMC Address/Data Bit 11)
  10	- 25	- GPIO_50 (GPMC Chip Select 0)
  11	- 26	- GPIO_56 (GPMC Address Valid/Address Latch Enable)
  12	- 27	- GPIO_51 (GPMC Chip Select 1)
  13	- 28	- GPIO_59 GPMC Byte Enable 0/Command Latch Enable)


  J3 Pin - Pin Name
  na  - 1 - 1.8V I/O Power
  na  - 2 - 5Vdc Input Power
  na  - 3 - GPMC Address/Data Bit 7
  14	- 4	- GPIO_140 (SPI1 Chip Select 3 (also UART1_RTS) )
  na  - 5 - GPMC Address/Data Bit 6
  15	- 6	- GPIO_156 (UART4 Transmit Data)
  na  - 7 - GPMC Address/Data Bit 5
  16	- 8	- GPIO_155 (UART4 Receive Data)
  na  - 9 - GPMC Address/Data Bit 4
  17	- 10	- GPIO_138 (SPI1 Chip Select 1 (also UART1_RX))
  na  - 11 - GPMC Address/Data Bit 3
  18	- 12	- GPIO_136 (SPI1 Slave In Master Out )
  na  - 13 - GPMC Address/Data Bit 2
  19	- 14	- GPIO_139 (SPI1 Chip Select 2 (also UART1_CTS))
  na  - 15 - GPMC Address/Data Bit 1
  20	- 16	- GPIO_137 (SPI1 Chip Select 0)
  na  - 17 - GPMC Address/Data Bit 0
  21	- 18	- GPIO_135 (SPI1 Slave Out Master In)
  na  - 19 - GPMC Write Enable
  22	- 20	- GPIO_134 (SPI1 Clock Out)
  na  - 21 - GPMC Output Enable
  23	- 22	- GPIO_39 (GPMC Address/Data Bit 15)
  24	- 23	- GPIO_133 (I2C4 Serial Data )
  25	- 24	- GPIO_132 (I2C4 Serial Clock)
  na  - 25 - TWL6030 REGEN1
  na  - 26 - Power On Reset
  na  - 27 - Digital Ground
  na  - 28 - Digital Ground
*/



// Pin (J6) / GPIO //
/*
	Pin 17 = GPIO 36
	Pin 18 = GPIO 32
	Pin 19 = GPIO 61
	Pin 20 = GPIO 33
	Pin 21 = GPIO 54
	Pin 22 = GPIO 34
	Pin 23 = GPIO 55
	Pin 24 = GPIO 35
	Pin 25 = GPIO 50
	Pin 26 = GPIO 56
	Pin 27 = GPIO 51
	Pin 28 = GPIO 59
*/

// Pin (J6) / PORT //
/*
	Pin 17 = PORT A6
	Pin 18 = PORT A7
	Pin 19 = PORT A4
	Pin 20 = PORT A5
	Pin 21 = PORT A2
	Pin 22 = PORT A3
	Pin 23 = PORT A0
	Pin 24 = PORT A1
	Pin 25 = PORT B1
	Pin 26 = PORT B0
	Pin 27 = PORT B3
	Pin 28 = PORT B2
*/
#include <stdlib.h>
#include <time.h>
#include <sched.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/io.h>
#include <string.h>

#define DATA_0			(0)	// PORT A0 / J6 Pin 23 / GPIO 55
#define DATA_1			(1)	// PORT A1 / J6 Pin 24 / GPIO 35
#define DATA_2			(2)	// PORT A2 / J6 Pin 21 / GPIO 54
#define DATA_3			(3)	// PORT A3 / J6 Pin 22 / GPIO 34
#define DATA_4			(4)	// PORT A4 / J6 Pin 19 / GPIO 61
#define DATA_5			(5)	// PORT A5 / J6 Pin 20 / GPIO 33
#define DATA_6			(6)	// PORT A6 / J6 Pin 17 / GPIO 36
#define DATA_7			(7)	// PORT A7 / J6 Pin 18 / GPIO 32

#define OP_0				(8)	// PORT B0 / J6 Pin 26 / GPIO 56
#define OP_1				(9)	// PORT B1 / J6 Pin 25 / GPIO 50
#define OP_TRIGGER		(10)	// PORT B2 / J6 Pin 28 / GPIO 59
#define OP_2				(11)	// PORT B3 / J6 Pin 27 / GPIO 51

#define OP_NULL			(0)	// 
#define OP_DEFINE		(1)	// 
#define OP_DEFINC		(2)	// 
#define OP_SETMSK		(3)	// 
#define OP_CLRMSK		(4)	// 
#define OP_SETADR		(5)	// 
#define OP_SETLRC		(6)	// 
#define OP_SETRGC		(7)	// 


// --- Filesystem operations ---

void writePin(int pinIndex, bool pinValue);
bool readPin(int pinIndex);


// --- Pin operations ---

void setTrigger(bool value);
void setOp(char opcode);
void setBus(char data);


// --- DPU operations ---

void write(char value);
void writeAndIncrement(char value);
void setMask(char value);
void clearMask(char value);
void setAddress(char value);
void setLayerCount(char value);
void setRegisterCount(char value);


// --- Draw operations ---

void setVoxel(int x, int y, int z, bool a);


// --- IO operations ---

void callOperation(char opcode, char value);
int openDpuIO(void);
int closeDpuIO(void);

#endif // DPU_IO_H