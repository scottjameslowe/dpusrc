app.directive('starbuck', function() {
	return {
		restrict: 'EA',
		scope: {
			
		},
		templateUrl: 'templates/starbuckTemplate.html',
		controller: function($scope, dpuBuffer, keyboardService){
			
			
			
			var framerate = 1000 / 25;
			var lastFrame = new Date();
			
			var timer;
			
			var key = keyboardService;
			
			var game_object_instance = 0;
			
			var gameObjects = [];
			
			var enemySeed = 1;
			var seedDelta = 0.01;
			
			
			
			
			var player = createGameObject("Player", 4, 0, 4);
			
			player.update = function()
			{
				
			};
			
			timer = setInterval(tick, 10);
			function tick()
			{
				var now = new Date();
				if(now - lastFrame >= framerate)
				{
					
					lastFrame = now;
					generateEnemies();
					update();
					draw();
				}
			  
			}
			
			
			function generateEnemies()
			{
				enemySeed += 0.01;
				if(Math.random() > enemySeed)
				{
					console.log(enemySeed);
					if(enemySeed < 0) {
						enemySeed = 0;
					}
					else {
						enemySeed -= seedDelta;
					}
					
					var randX = Math.random() * 7;
					var randY = 7;
					var randZ = Math.random() * 7;
					
					createEnemy(5, randX, randY, randZ);
				}
			}
			
			function update()
			{
				var deleteQueue = [];
				
				for(var i in gameObjects){
					if(gameObjects[i].shouldDestroy()) {
						deleteQueue.push(i);
					}
					else {
						gameObjects[i].update();
					}
				}
				
				for(var i = 0; i < deleteQueue.length; i++)
				{
					delete gameObjects[deleteQueue[i]];
				}
			}
			
			function draw()
			{
				dpuBuffer.fill(0x00);
				for(var i in gameObjects)
				{
					var v = gameObjects[i];
					dpuBuffer.setVoxel(v.x, v.y, v.z, true);
				}
				
				dpuBuffer.draw();
			}
			
			
			this.start = function(){
				
				
				if(timer == null){
					timer = setInterval(tick, 10);
				}
			};
			
			this.stop = function(){
			
				
				if(timer != null)
				{
					clearInterval(timer);
					timer = null;
				}
				
			};
			
			
			$scope.buttonClick = function(button)
			{
				console.log(button);
			
				switch(button)
				{
					case 'a':
					case 'b':
						fire();
						break;
						
					case 'left':
						moveLeft();
						break;
						
					case 'right':
						moveRight();
						break;
						
					case 'up':
						moveUp();
						break;
						
					case 'down':
						moveDown();
						break;
				}
			}
			
			key.registerPress(key.LEFT, [], moveLeft);
			
			key.registerPress(key.RIGHT, [], moveRight);
			
			key.registerPress(key.UP, [], moveUp);
			
			key.registerPress(key.DOWN, [], moveDown);
			
			key.registerPress(key.SPACE, [], fire);
			
			function moveLeft(){
				if(player.x > 0) { player.x--; }
			}
			
			function moveRight(){
				if(player.x < 7) { player.x++; }
			}
			
			function moveUp(){
				if(player.z < 7) { player.z++; }
			}
			
			function moveDown(){
				if(player.z > 0) { player.z--; }
			}
			
			function fire(){
				createBullet( 10, player.x, player.y, player.z );
			}
			
			
			function createEnemy(speed, x, y, z){
				
				var direction = {
					x:0,
					y:-1,
					z:0
				}
			
				var timestamp = new Date();
				var benchmark = 1000/speed;
			
				var enemy = createGameObject("enemy", x, y, z);
				
				enemy.update = function(){
					
					
					var tick = new Date();
					if((tick - timestamp) < benchmark) return;
					timestamp = tick;
					
					enemy.x += direction.x;
					enemy.y += direction.y;
					enemy.z += direction.z;
					
					if(	enemy.x > 7 || enemy.x < 0 
					||	enemy.y > 7 || enemy.y < 0
					||	enemy.z > 7 || enemy.z < 0 )
					{
						enemy.destroy();
					}	
					
				};
			
			}
			
			function createBullet(speed, x, y, z){
			
				var direction = {
					x:0,
					y:1,
					z:0
				}
			
				var timestamp = new Date();
				var benchmark = 1000/speed;
				
				var bullet = createGameObject("bullet", x, y, z);
				
				bullet.update = function (){
				
					var tick = new Date();
					if((tick - timestamp) < benchmark) return;
					timestamp = tick;
					
					bullet.x += direction.x;
					bullet.y += direction.y;
					bullet.z += direction.z;
					
					if(	bullet.x > 7 || bullet.x < 0 
					||	bullet.y > 7 || bullet.y < 0
					||	bullet.z > 7 || bullet.z < 0 )
					{
						bullet.destroy();
					}
				};
				
				return bullet;
				
			}
			
			function createGameObject(name, x, y, z){
				
				var shouldDestroy = false;
				
				var gobj = {
					id : game_object_instance++,
					name : name? name : "undefined",
					destroy:false,
					
					x : (x != null)? x : 0,
					y : (y != null)? y : 0,
					z : (z != null)? z : 0,
					
					update : function(){},
					destroy : function(){
						gobj.update = function(){};
						shouldDestroy = true;
					},
					shouldDestroy : function(){ return shouldDestroy; }
					
				};
				
				gameObjects[gobj.id] = gobj;
				return gobj;
			
			};
			
			
			
			
		}
	};
});

