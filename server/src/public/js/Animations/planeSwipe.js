
function planeSwipe()
{
	var aniObj = {};

	aniObj.framerate = 25;
	aniObj.framecount = 0;
	aniObj.delta = 0;
	
	var x = 0;
	var y = 0;
	var z = 0;
	
	aniObj.update = function(){
		
		if(++aniObj.delta < 25) { return; }
		aniObj.delta = 0;
		
		aniObj.buffer.clear();
		
		aniObj.buffer.setVoxel(x,y,z,true);
		aniObj.buffer.setVoxel((7-x),(7-y),(7-z),true);
		
		if(++x > 7)
		{
			if(++y > 7)
			{
				if(++z > 7)
				{
					z = 0;
				}
				y = 0;
			}
			x = 0;
		}
		
	};
	
	
	return aniObj;
}

