
var dpu = dpu || {};

dpu.graphics = dpu.graphics || {};

dpu.graphics.create

(function(){





	dpu.graphics.createBuffer = function()
	{


		return {


		};

		function flush()
		{
			
			
		}
	}

});






function copyBuffer(dpuBuffer buffer)
{
	byte y = 7;
	byte z = 7;

	do
	{
		do
		{
			drawBuffer[y][z] = buffer[y][z];
		}
		while(z--);
	}
	while(y--);
}

void compositeBuffer(dpuBuffer buffer)
{
	byte y = 7;
	byte z = 7;

	do
	{
		do
		{
			drawBuffer[y][z] |= buffer[y][z];
		}
		while(z--);
	}
	while(y--);
}

void flipBuffer(dpuBuffer buffer)
{
	byte y = 7;
	byte z = 7;

	do
	{
		do
		{
			drawBuffer[y][z] ^= buffer[y][z];
		}
		while(z--);
	}
	while(y--);
}


void setVoxel(int x, int y, int z)
{
	drawBuffer[y][z] |= (0x01<<x);
}

void clearVoxel(int x, int y, int z)
{
	drawBuffer[y][z] &= ~(0x01<<x);
}

void writeVoxel(int x, int y, int z, int value)
{
	if (value)	setVoxel(x,y,z);
	else		clearVoxel(x,y,z);
}

byte getVoxel(int x, int y, int z)
{
	if (drawBuffer[y][z] & (1 << x)) return 0x01;
	else return 0x00;
}

void flipVoxel(int x, int y, int z)
{
	drawBuffer[y][z] ^= (1 << x);
}

void setLine(int x1, int y1, int z1, int x2, int y2, int z2)
{
	float xy;	// how many voxels do we move on the y axis for each step on the x axis
	float xz;	// how many voxels do we move on the y axis for each step on the x axis 
	unsigned char x,y,z;
	unsigned char lasty,lastz;

	// We always want to draw the line from x=0 to x=7.
	// If x1 is bigget than x2, we need to flip all the values.
	if (x1>x2)
	{
		int tmp;
		tmp = x2; x2 = x1; x1 = tmp;
		tmp = y2; y2 = y1; y1 = tmp;
		tmp = z2; z2 = z1; z1 = tmp;
	}

	
	if (y1>y2)
	{
		xy = (float)(y1-y2)/(float)(x2-x1);
		lasty = y2;
	} else
	{
		xy = (float)(y2-y1)/(float)(x2-x1);
		lasty = y1;
	}

	if (z1>z2)
	{
		xz = (float)(z1-z2)/(float)(x2-x1);
		lastz = z2;
	} else
	{
		xz = (float)(z2-z1)/(float)(x2-x1);
		lastz = z1;
	}



	for (x = x1; x<=x2;x++)
	{
		y = (xy*(x-x1))+y1;
		z = (xz*(x-x1))+z1;
		setVoxel(x,y,z);
	}
	
}

void setPlaneZ (int z)
{
	if (z < 0 || z > 7) return;

	for (byte y=0; y<8; y++)
		drawBuffer[y][z] = 0xff;
}


void clearPlaneZ (int z)
{
	if (z < 0 || z > 7) return;

	for (byte y=0; y<8; y++)
		drawBuffer[y][z] = 0x00;
}

void setPlaneX (int x)
{
	if (x < 0 || x > 7) return;

	for (byte z=0; z<8; z++)
	{
		for (byte y=0; y<8; y++)
		{
			drawBuffer[y][z] |= (1 << x);
		}
	}
}

void clearPlaneX (int x)
{
	if (x < 0 || x > 7) return;

	for (byte z=0; z<8; z++)
	{
		for (byte y=0; y<8; y++)
		{
			drawBuffer[y][z] &= ~(1 << x);
		}
	}
}

void setPlaneY (int y)
{
	if (y < 0 || y > 7) return;

	for (byte z=0; z<8; z++)
		drawBuffer[y][z] = 0xff;
}

void clearPlaneY (int y)
{
	if (y < 0 || y > 7) return;

	for (byte z=0; z<8; z++)
		drawBuffer[y][z] = 0x00;
}


void fill (byte pattern)
{
	for (byte y=0;y<8;y++)
	{
		for (byte z=0;z<8;z++)
		{
			drawBuffer[y][z] = pattern;
		}
	}
}


void copyCell(dpuCell origin, dpuCell destination)
{
	

}