var dpu = dpu || {};

(function(){

	var socket = io.connect('http://192.168.1.100');

	var nucleus;

	var isEnabled;
	var isActive;

	socket.on('activeUpdate', function(active){
		console.log("SERVER:activeUpdate", active);
		isActive = active;
	});

	socket.on('enabledUpdate', function(enabled){
		console.log("SERVER:enabledUpdate", enabled);
		isEnabled = enabled;
	});

	socket.on('nucleusUpdate', function(nucleusData){
		console.log("SERVER:nucleusUpdate", nucleusData);
		nucleus = nucleusData;
	});

	socket.on('voxelUpdate', function(x, y, z, a){
		console.log("SERVER:voxelUpdate", x, y, z, a);
		nucleus[x][y][z] = a;
	});

	function requestUpdate()
	{

	}

	function getActive()
	{
		return isActive;
	}

	function setActive(active)
	{
		isActive = active;
		socket.emit('setEnabled', active);
	}

	function getEnabled()
	{
		return isEnabled;
	}

	function setEnabled(enabled)
	{
		isEnabled = enabled;
		socket.emit('setEnabled', enabled);
	}

	function drawVoxel(x, y, z, a)
	{
		socket.emit('drawVoxel', x, y, z, a);
	}

	function getVoxel(x, y, z)
	{
		return nucleus[x][y][z];
	}

	function toggleVoxel(x, y, z)
	{
		drawVoxel(x, y, z, (!getVoxel(x, y, z)) );
	}


	dpu.getActive = getActive;
	dpu.setActive = setActive;

	dpu.getEnabled = getEnabled;
	dpu.setEnabled = setEnabled;

	dpu.drawVoxel = drawVoxel;
	dpu.getVoxel = getVoxel;
	dpu.toggleVoxel = toggleVoxel;

})();

var exampleAnimation = [
	{
		timestamp: 0.2,
		action: function() { dpu.toggleVoxel(0, 1, 1); }
	}
];