app.directive('debugger', function() {
	return {
		restrict: 'EA',
		scope: {
			
		},
		templateUrl: '/templates/debuggerTemplate.html',
		controller: function($scope, $element, dpuBuffer, keyboardService){
			
			dpuBuffer.fill(0x0f);
			dpuBuffer.draw();
			
			var bufferModel = [];
			
			for(var x = 0; x < 8; x++)
			{
				for(var y = 0; y < 8; y++)
				{
					for(var z = 0; z < 8; z++)
					{
						if(bufferModel[z] == null) bufferModel[z] = [];
						if(bufferModel[z][y] == null) bufferModel[z][y] = [];
						if(bufferModel[z][y][x] == null)
						{
							bufferModel[z][y][x] = {
								alpha: dpuBuffer.getVoxel(x,y,z),
								x: x,
								y: (7-y),
								z: (7-z)
							};
						}
					}
				}
			}
			
			var currentZ = 0;
			
			var z_layers = [];
			
			// draw params
			var nodeRadius = 15;
			
			var selection = {
				active:false,
				start:{
					x:0,
					y:0
				},
				end:{
					x:0,
					y:0
				},
				area:{
					top:0,
					bottom:0,
					left:0,
					right:0
				},
				
				contains: function(rect) {
				  return !(rect.left > area.right || 
						   rect.right < area.left || 
						   rect.top > area.bottom ||
						   rect.bottom < area.top);
				}
			};
			
			
			
			var root = d3.select('#debugger');
			
			var svg = root.append("svg");
			
			svg
				.attr("width",1024)
				.attr("height", 700)
			;
			
			svg
				.on("mousedown", function(){
					console.log("mousedown", d3.event);
					selection.start.x = d3.event.layerX;
					selection.start.y = d3.event.layerY;
					selection.active = true;
					selectionRect
						.attr("opacity", 0.8)
					;
				})
				.on("mouseup", function(){
					console.log("mouseup", d3.event);
					selection.active = false;
					selectionRect
						.attr("opacity", 0)
					;
					svgGroup.selectAll("circle").each(function(d){
						console.log(d3.select(this));
					});
					
				})
				.on("mousemove", function(){
					if(!selection.active) return;
					
					console.log("mousemove", d3.event);
					
					selection.end.x = d3.event.layerX;
					selection.end.y = d3.event.layerY;
					
					if(selection.start.x < selection.end.x)
					{
						selection.area.left = selection.start.x;
						selection.area.right = selection.end.x;
					}
					else
					{
						selection.area.left = selection.end.x;
						selection.area.right = selection.start.x;
					}
					
					if(selection.start.y < selection.end.y)
					{
						selection.area.top = selection.start.y;
						selection.area.bottom = selection.end.y;
					}
					else
					{
						selection.area.top = selection.end.y;
						selection.area.bottom = selection.start.y;
					}
					
					selectionRect
						.attr("x", selection.area.left)
						.attr("y", selection.area.top)
						.attr("width", selection.area.right - selection.area.left)
						.attr("height", selection.area.bottom - selection.area.top)
					;
					
				})
			;
			
			var svgGroup = svg.append("g");
			
			var svgSelectionArea = svg.append("g");
			
			var selectionRect = svgSelectionArea.append("rect")
				.attr("stroke", "#333333")
				.attr("stroke-width", 4)
				.attr("fill", "#663399")
				.attr("opacity", 0)
			;
			
			svgGroup
				.attr("transform", function(d,i){ return "translate(" + 40 + "," + 40 +")" })
				
				
				
			console.log(root);
			console.log(bufferModel);
			
			var nodes = svgGroup.selectAll("g")
				.data(bufferModel)
				.enter()
				.append("g")
				
				
				.attr("transform", function(d,i){ return "translate(" + nodeRadius/2*i + "," + nodeRadius/2*i +")" })
				
				
				
				.selectAll("g")
				.data(function(d){ return d; })
				.enter()
				.append("g")
				.attr("transform", function(d,i){ return "translate(" + 0 + "," + (nodeRadius * 2.5)*i +")" })
				.selectAll("circle")
				.data(function(d){ return d; })
				.enter()
				.append("circle")
				.style("cursor", "pointer")
				.attr("cx", function(d,i){ return (nodeRadius * 2.5)*i; } )
				.attr("fill", '#9999cc' )
				.attr("fill-opacity", function(d){ return d.alpha? 1 : 0; })
				.attr("r", nodeRadius)
				.attr("stroke", "#000000")
				.attr("stroke-width", 2)
				.on("click", function(d){
					
					//if(d.z != currentZ) return;
					
					d.alpha = !d.alpha;
					d3.select(this)
						.attr("fill-opacity", function(d){ return d.alpha? 1 : 0; })
					;
					
					dpuBuffer.setVoxel(d.x, d.y, d.z, d.alpha);
					dpuBuffer.draw();
				})
			;
			console.log(z_layers);
			setZ(2);
			
			function setZ(zIndex)
			{
				currentZ = zIndex;
				svgGroup.selectAll("circle").each(function(d,i)
				{
					var isActive = (d.z == currentZ);
				
					d3.select(this)
						.style("opacity", isActive? 1 : 0.25)
						.style("pointer-events", isActive? "all" : "none")
						.style("stroke-dasharray", d.z < currentZ? '"5,5" d="M5 20 l215 0"' : "")
					;
				});
				console.log("SET Z", zIndex);
			}
			
			keyboardService.registerPress(keyboardService.UP, [], function(){
				var z = currentZ+1;
				if(z > 7)	setZ(0);
				else		setZ(z);
				
			});
			
			keyboardService.registerPress(keyboardService.DOWN, [], function(){
				var z = currentZ-1;
				if(z < 0)	setZ(7);
				else		setZ(z);
			});
			
		}
	};
});