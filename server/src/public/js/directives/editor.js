app.directive('editor', function() {
	return {
		restrict: 'EA',
		scope: {
			
		},
		templateUrl: 'templates/editorTemplate.html',
		controller: function($scope, dpuService, keyboardService){
			
			
			var dpu = dpuService;
			var key = keyboardService;
			
			var STATE = {};
			STATE.CURSOR = "cursor";
			STATE.SELECTION = "selection";
			STATE.CLIPBOARD = "clipboard";
			STATE.TRANSFORM = "transform";
			STATE.PLAYING = "playing";
			
			var state = STATE.CURSOR;
			
			var clipboard;
			
			var selectionBuffer = dpu.createNucleus(false);
			
			
			var currentFrame = 0;
			
			var frames = [dpu.createNucleus(false)];
			
			
			var drawBuffer = dpu.createNucleus(false);
			setTimeout(updateDpu, 1000);
			
			var selection;
			var selectionFill = true;
			
			function getDefaultSelection()
			{
				return {
					start:{ x:0, y:0, z:0 },
					end:{ x:7, y:7, z:7  }
				};
			}
			
			var cursor = {
				x:5,
				y:4,
				z:4
			};
			
			function updateDpu()
			{
				dpu.drawNucleus( dpu.combine([drawBuffer, selectionBuffer, frames[currentFrame]]) );
			}
			
			function setClipboard(nucleus, origin)
			{
				clipboard = {};
				clipboard.origin = origin;
				clipboard.data = nucleus;
				clipboard.a = setInterval(function(){
					dpu.drawNucleus(clipboard.data);
				}, 800);
				
				setTimeout(function(){
					clipboard.b = setInterval(function(){
					updateDpu();	
					}, 800);
				}, 400);
			}
			
			function clearClipboard()
			{
				if(clipboard){
					clearTimeout(clipboard.a);
					clearTimeout(clipboard.b);
				}
			
				clipboard = null;
			}
			
			function clearSelection()
			{
				selection = null;
				selectionBuffer = dpu.createNucleus(false);
			}
			
			
			key.registerPress(key.ENTER, [], function(){
				switch(state)
				{
					case STATE.CURSOR:
					
						var alpha = !frames[currentFrame][cursor.x][cursor.y][cursor.z];
						frames[currentFrame][cursor.x][cursor.y][cursor.z] = alpha;
						updateDpu();
						break;
					
					case STATE.SELECTION:
						
						dpu.setRegion(frames[currentFrame], selection.start, selection.end, selectionFill);
						selectionFill = !selectionFill;
						updateDpu();
						break;
						
					case STATE.TRANSFORM:
					
						// apply transform
						break;
						
					case STATE.CLIPBOARD:
						
						// apply clipboard
						break;
				}
				
				if(clipboard) {
					frames[currentFrame] = dpu.combine([frames[currentFrame], clipboard.data]);
					clearClipboard();
					updateDpu();
					return;
				}
			});
			
			
			
			// normal mode
			key.setMode(key.NONE, {
				onPress: function(keycode)
				{
					console.log("NORMAL MODE PRESS");
					
					var vector = {x:0,y:0,z:0};
					var distance = 1;
					
					switch(keycode)
					{
						case key.ENTER:
							
							
							break;
						
				key.registerKeypress(key.ESC, [], function(){
							
					switch(state)
					{
						case STATE.SELECTION:
							clearSelection();
							state = STATE.CURSOR;
							updateDpu();
							return;
								
						case STATE.CLIPBOARD:
							
							clearClipboard();
							return;
					}
				}
						
						
						
						
				key.registerKeypress(key.LEFT, [], fuction(){
							
					vector.x = -distance;
				});
							
				key.registerKeypress(key.RIGHT, [], fuction(){
							
					vector.x = distance;
				});
					

				key.registerKeypress(key.UP, [key.CTRL], fuction(){
							
					vector.y = distance;
				});
					
				key.registerKeypress(key.UP, [], fuction(){
							
					vector.z = distance;
				});
							
				key.registerKeypress(key.DOWN, [key.CTRL], fuction(){
							
					vector.y = -distance;
				});
							
				key.registerKeypress(key.DOWN, [], fuction(){
							
					vector.z = -distance;
				});
						
						
						
						
						
						// COMMAND KEYS //
						
						// Ctrl-C
						case key.C:
							
							if(!key.isDown(key.CTRL)) { break; }
							
							switch(state)
							{
								case STATE.SELECTION:
									
									var clipData = dpu.copyRegion(frames[currentFrame], selection.start, selection.end);
									setClipboard(clipData, cursor);
									break;
							}
							break;
						
						// Ctrl-X
						case key.X:
							
							if(!key.isDown(key.CTRL)) { break; }
							
							switch(state)
							{
								case STATE.SELECTION:
									
									var clipData = dpu.copyRegion(frames[currentFrame], selection.start, selection.end);
									setClipboard(clipData, cursor);
									break;
							}
							break;
							
						// Ctrl-V
						case key.V:
							
							if(!key.isDown(key.CTRL)) { break; }
							
							state = STATE.CLIPBOARD;
							
							
							break;
						
						// Ctrl-A
						case key.A:
							
							// selection = 
							// updateDpu();
							break;
						
						
						
						// TIMELINE CONTROLS //
						
						case key.INSERT:
							
							frames.splice(currentFrame, 0, dpu.createNucleus(false));
							updateDpu();
							
							break;
						
						case key.DELETE:
							
							switch(state)
							{
								case STATE.CURSOR:
									if(frames.length > 1)
									{
										frames.splice(currentFrame, 1);
										if(currentFrame >= frames.length){
											currentFrame = frames.length -1;
										}
										updateDpu();
									}
									break;
								case STATE.SELECTION:
									frames[currentFrame] = dpu.fillRegion(frames[currentFrame], selection, false);
									
									break;
							}
							frames[currentFrame] = dpu.createNucleus(false);
							updateDpu();
							
							break;
						
						case key.HOME:
							
							currentFrame = 0;
							updateDpu();
							break;
							
						case key.END:
							
							currentFrame = frames.length -1;
							updateDpu();
							break;
						
						case key.PAGEDOWN:
							
							if(currentFrame > 0)
							{
								currentFrame--;
								updateDpu();
							}
							
							break;
					
						case key.PAGEUP:
							
							currentFrame++;
							
							if(currentFrame >= frames.length)
							{
								frames.push(dpu.createNucleus(false));
							}
							
							updateDpu();
							
							break;
							
							
						case key.SPACE:
							
							STATE = STATE.PLAYING;
							
							
							break;
						
					}
					
					
					switch(state)
					{
						case STATE.CURSOR:
						
							cursor.x += vector.x;
							cursor.y += vector.y;
							cursor.z += vector.z;
							
							if(cursor.x < 0) { cursor.x = 0; }
							if(cursor.x > 7) { cursor.x = 7; }
							
							if(cursor.y < 0) { cursor.y = 0; }
							if(cursor.y > 7) { cursor.y = 7; }
							
							if(cursor.z < 0) { cursor.z = 0; }
							if(cursor.z > 7) { cursor.z = 7; }
							
							
							drawBuffer = dpu.createNucleus(false);
							drawBuffer[cursor.x][cursor.y][cursor.z] = true;
							updateDpu();
							break;
							
						case STATE.SELECTION:
						
							selection.start.x += vector.x;
							selection.end.x += vector.x;
							
							selection.start.y += vector.y;
							selection.end.y += vector.y;
							
							selection.start.z += vector.z;
							selection.end.z += vector.z;
							
							var diff;
							
							if(selection.start.x < 0) {
								diff = 0 - selection.start.x;
								selection.start.x += diff;
								selection.end.x += diff;
							}
							if(selection.end.x > 7) {
								diff = selection.start.x - 7;
								selection.start.x -= diff;
								selection.end.x -= diff;
							}
							
							if(selection.start.y < 0) {
								diff = 0 - selection.start.y;
								selection.start.y += diff;
								selection.end.y += diff;
							}
							if(selection.end.y > 7) {
								diff = selection.start.y - 7;
								selection.start.y -= diff;
								selection.end.y -= diff;
							}
							
							if(selection.start.z < 0) {
								diff = 0 - selection.start.z;
								selection.start.z += diff;
								selection.end.z += diff;
							}
							if(selection.end.z > 7) {
								diff = selection.start.z - 7;
								selection.start.z -= diff;
								selection.end.z -= diff;
							}
							
							
							selectionBuffer = dpu.drawBounds(selection.start, selection.end);
							updateDpu();
						
							break;
					}
					
					
					if(clipboard)
					{
						var bounds = getDefaultSelection();
						clipboard.origin = cursor;
						clipboard.data = dpu.moveRegion(clipboard.data, bounds.start, bounds.end, vector);
					}
				}
				
				
				
				
				
				
			});
			
			key.setMode(key.CTRL, {
			onPress: function(keycode)
				{
					console.log("SELECTION MODE PRESS");
					
			var distance = 1;
					
						
			key.registerPress(key.LEFT, [key.SHIFT], function(){
				
				selection.end.x -= distance;
				if(selection.end.x < 0) { selection.end.x = 0; }
				
			});
							
								
			key.registerPress(key.RIGHT, [key.SHIFT], function(){
						
				selection.end.x += distance;
				if(selection.end.x > 7) { selection.end.x = 7; }
			});
							
			key.registerPress(key.UP, [key.SHIFT], function(){

				selection.end.z += distance;
				if(selection.end.z > 7) { selection.end.z = 7; }
			});
			
				
			key.registerPress(key.UP, [key.SHIFT, key.CTRL], function(){

				selection.end.y += distance;
				if(selection.end.y > 7) { selection.end.y = 7; }
			});

			
			key.registerPress(key.DOWN, [key.SHIFT], function(){
						
				selection.end.z -= distance;
				if(selection.end.z < 0) { selection.end.z = 0; }
				
				selection.end.y -= distance;
				if(selection.end.y < 0) { selection.end.y = 0; }
							
			});
					
					selectionBuffer = dpu.drawBounds(selection.start, selection.end);
					updateDpu();
					
				
			});
			
			
				onStart: function()
				{
					console.log("SELECTION MODE START");
					
					if(selection == null)
					{
						selection = {
							start: {
								x: cursor.x,
								y: cursor.y,
								z: cursor.z
							},
							end: {
								x: cursor.x,
								y: cursor.y,
								z: cursor.z
							}
						};
					}
				},
				onEnd: function()
				{
					if(	selection != null &&
						selection.start.x == selection.end.x &&
						selection.start.y == selection.end.y &&
						selection.start.z == selection.end.z )
					{
						clearSelection();
					}
					else
					{
						state = STATE.SELECTION;
					}
					console.log("SELECTION MODE END");
				}
			});
			
			
			
			// translation mode
			key.setMode(key.M, {
				onPress: function (keycode)
				{
					var vector = {x:0,y:0,z:0};
					var distance = (key.isDown(key.SHIFT))? 5 : 1;
					
					switch(keycode)
					{
						case key.LEFT:
							
							vector.x = -distance;
							break;
							
						case key.RIGHT:
						
							vector.x = distance;
							break;
							
						case key.UP:
							
							if(key.isDown(key.CTRL))	{ vector.y = distance; }
							else						{ vector.z = distance; }
							break;
							
						case key.DOWN:
						
							if(key.isDown(key.CTRL))	{ vector.y = -distance; }
							else						{ vector.z = -distance; }
							break;
							
						default: return;
					}
					
					
					var bounds = (selection != null)? selection : getDefaultSelection();
					
					frames[currentFrame] = dpu.moveRegion(frames[currentFrame], bounds.start, bounds.end, vector); 
					updateDpu();
				},
				onRelease: function(keycode)
				{
					
				},
				onStart: function()
				{
					
				},
				onEnd: function()
				{
					
				}
			});
			
			setInterval(update, 20);


			// var timeMark = new Date();

			function update()
			{
				// var tick = new Date();
				
				// if( (tick - timeMark) > 

			  if(state == STATE.PLAYING)
			  {
				if(++currentFrame >= frames.length) { currentFrame = 0; }
				dpuUpdate();
			  }
			}

			
		}
		
	};
});