var app = angular.module('dpuApp', []);

app.controller('dpuController', function($scope, dpuBuffer, keyboardService)
{
	$scope.modes = [
		{ name:'debugger'},
		{ name:'animator'},
		{ name:'starbuck'},
		{ name:'battleship'}
	];
		
	$scope.mode = $scope.modes[0];
	
	$scope.setMode = function(mode)
	{
		$scope.mode = mode;
	}
	
	$scope.isSelected = function(mode){
		return $scope.mode == mode;
	}
	
	console.log(keyboardService.LEFT);
	
	return;
	
	var tmp2 = dpuBuffer;
	var tmp = tmp2.clone();
		
		var idx = 0;
		var idy = 0;
		var idz = 0;
		var ida = true;
		
	setInterval(function(){
		return;
		tmp.fillRegion({x:0,y:0,z:0}, {x:idx,y:idy,z:idz}, ida);
		tmp.draw();	
		
		idx++;
		if(idx > 7){ 
			idx = 0;
			idy++;
			if(idy > 7){
				idy = 0;
				idz++;
				if(idz > 7){
					idz = 0;
					ida = !ida;
				}
			}
		}
		
	}, 2);
	
	return;
	
	var keyMap = [
		{ key:keyboardService.UP, modifiers:[], handler:"moveUp" }
		
	];
	
	console.log(keyMap);
	
	for(var i in keyMap)
	{
		console.log(i, keyMap[i]);
		keyboardService.registerPressHandler(
			keyMap[i].key,
			keyMap[i].modifiers,
			function(){
				console.log("press", keyMap[i].handler);
				currentState[keyMap[i].handler]();
			});
	}
	
	var cursor = {
		moveUp: function() {
			console.log("cursor state press");
			currentState = selection;
		}
	};
	
	
	var selection = {
		moveUp: function() {
			console.log("selection state press");
			currentState = cursor;
		}
	};
	
	var currentStates = selection;
	
	// keyboard
	
	
	
	// Axis/Defs:
	
	// Up/Down				=	Y+/Y-
	// Right/Left				=	X+/X-
	// Backward/Forward	=	Z+/Z-
	
	
	
	// KEYBOARD
	
	// MODES:
	
	// Cursor
	// Selection
	// Paste
	// Transform
	// Playback
	
	
	
	//   TRIGGERS | ACTIONS   //
	
	
	
	
	// MOVE //
	
	// Move Y+ (Ctrl-Up)
		// [Cursor] - Move cursor position up
		// [Selection] - Move selection area up
		// [Paste] - Move clipboard area up
		
	// key.registerPressHandler(key.UP, [key.CTRL], function(){ state.moveUp(); });
		
	// Move Y- (Ctrl-Down)
		// [Cursor] - Move cursor position down
		// [Selection] - Move selection area down
		// [Paste] - Move clipboard area down
	
	// key.registerPressHandler(key.DOWN, [key.CTRL], function(){ state.moveDown(); });
	
	// Move X+ (Right)
		// [Cursor] - Move cursor position right
		// [Selection] - Move selection area right
		// [Paste] - Move clipboard area right
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
	
	// Move X- (Left)
		// [Cursor] - Move cursor position left
		// [Selection] - Move selection area left
		// [Paste] - Move clipboard area left
		
	// key.registerPressHandler(key.LEFT, [], function(){ state.moveLeft(); });
	
	// Move Z+ (Down)
		// [Cursor] - Move cursor position forward
		// [Selection] - Move selection area forward
		// [Paste] - Move clipboard area forward
		
	// key.registerPressHandler(key.DOWN, [], function(){ state.moveForward(); });
		
	// Move Z- (Up)
		// [Cursor] - Move cursor position backward
		// [Selection] - Move selection area backward
		// [Paste] - Move clipboard area backward
		
	// key.registerPressHandler(key.UP, [], function(){ state.moveBackward(); });
	
	
	
	
	// SELECT AREA //
	
	// Move Selection Anchor Y+ (Shift-Ctrl-Up)
		// [Cursor, Selection] - Move end anchor point of selection area up
		
	// key.registerPressHandler(key.UP, [key.SHIFT, key.CTRL], function(){ state.moveRight(); });
	
	// Move Selection Anchor Y- (Shift-Ctrl-Down)
		// [Cursor, Selection] - Move end anchor point of selection area down
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
		
	// Move Selection Anchor X+ (Shift-Right)
		// [Cursor, Selection] - Move end anchor point of selection area right
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
		
	// Move Selection Anchor X- (Shift-Left)
		// [Cursor, Selection] - Move end anchor point of selection area left
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
		
	// Move Selection Anchor Z+ (Shift-Down)
		// [Cursor, Selection] - Move end anchor point of selection area forward
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
		
	// Move Selection Anchor Z- (Shift-Up)
		// [Cursor, Selection] - Move end anchor point of selection area backward
		
	// key.registerPressHandler(key.RIGHT, [], function(){ state.moveRight(); });
	
	
	// CLIPBOARD //
	
	// Copy Selected Area (Ctrl-C)
		// [Selection] - Copies the selected area to the clipboard, exits selection mode
		
	// Copy Selected Area (Ctrl-X)
		// [Selection] - Copies the selected area to the clipboard, clears the selected area, exits selection mode
		
	// Paste Clipboard (Ctrl-V)
		// [Cursor] - Enters paste mode
		
	
	
	
	// APPLY //
	
	// Toggle On/Off (Enter)
		// [Cursor] - Turns the current voxel at cursor on or off
		// [Selection] - Turns the voxels in the selected area on or off
		// [Paste] - Applies clipboard data to selected area
		// [Transform] - Applies transform
	
	
	// Quit Current Mode (Esc)
		// [Selection] - Clears the selected area
		// [Paste] - Cancels the current paste opperation
		// [Transform] - Cancels the current transform operation
	
	// Undo Last Action (Ctrl-Z)
		// [Cursor, Selection, Transform. Paste] - Undo the last change, step backward through change history
	
	// Redo Last Action (Ctrl-Shift-Z)
		// [Cursor, Selection, Transform, Paste] - Redo the last change, step forward through change history
		
		
		
		
	
	
	
	// TRANSFORM - ROTATE //
	
	
	// Rotate Left (R-Left)
		// [Selection] - Rotate selection area counter-clockwise around the (global) Y Axis
	
	// Rotate Right (R-Right)
		// [Selection] - Rotate selection area clockwise around the (global) Y Axis
	
	// Rotate Up (R-Up)
		// [Selection] - Rotate selection area clockwise around the (local) X Axis
	
	// Rotate Down (R-Down)
		// [Selection] - Rotate selection area clockwise around the (local) X Axis
		
	
	
	// TRANSFORM - SCALE //
	
	// Scale Up (S-Up)
		// [Selection] - Scale selection area up
		
	// Scale Down (S-Down)
		// [Selection] - Scale selection area down
	
	
	
	
	
	// TRANSFORM - TRANSLATE //
	
	// Translate Up (T-Ctrl-Up)
		// [Selection] - Translate selection area up
	
	// Translate Down (T-Ctrl-Down)
		// [Selection] - Translate selection area down
		
	// Translate Left (T-Left)
		// [Selection] - Translate selection area left
		
	// Translate Right (T-Right)
		// [Selection] - Translate selection area right
		
	// Translate Forward (T-Up)
		// [Selection] - Translate selection area forward
		
	// Translate Backward (T-Down)
		// [Selection] - Translate selection area backward
	
	
	
	
	// PLAYBACK //
	
	// Play (Shift)
		// [Cursor, Selection, Paste, Transform] - Starts playback from the current frame
	
	// Stop (Shift)
		// [Playback] - Stops playback, seeks to last frame before playback started, resumes last mode/operation before playback starts
	
	// Accelerate Playback Speed (PageUp)
		// [Playback] - Accelerated the framerate of the current playback
	
	// Slow Playback Speed (PageDown)
		// [Playback] - Slows the framerate of the current playback
		
		
	
	// TIMELINE //
	
	// Insert New Frame (Insert)
		// [Cursor] - Adds a new frame at current frame index, pushes current and all future frames forward by 1 frame
		
	// Delete Current Frame (Delete)
		// [Cursor] - Deletes the frame at the current index, pulls all future frames backward by 1 frame
	
	// Seek to Beginning (Home)
		// [Cursor] - Jump to the first frame in the current animation
	
	// Seek to End (End)
		// [Cursor] - Jump to the last frame in the current animation
		
	// Go to Next Frame (PageUp)
		// [Cursor] - Jumps to the next frame in the animaion, if there is a next frame
		
	// Go to Previous Frame (PageDown)
		// [Cursor] - Jumps to the previous frame in the animation, if there is a previous frame
	
	
	
	// SWITCH STATE //
	
	// Edit State (F1)
	
	// Procedural Animations (F2)
	
	// Games (F3)
	
	
	
	
	
	
	
	
	
	
	
	
	
	// leap
	
	// SINGLE HAND //
	
	// Select Cursor (Pointer Finger)
		// [Cursor] - Moves cursor position as directed by pointer finger
		
		
	// Select Area (Pointer Fingers)
		// [Selection] - Left/Right pointer fingers determine anchor points
	
	
	
	
	
	
	
	// ui
	
	
	
	// database
	
	// animation playback
	
	// procedural animations

	// message system
	
	
	// games
	
		// galaga



});