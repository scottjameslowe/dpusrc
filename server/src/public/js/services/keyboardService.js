app.service('keyboardService', function(){

	this.NONE		= 0;
	
	this.INSERT		= 45;
	this.DELETE		= 46;
	this.HOME		= 36;
	this.END		= 35;
	this.PAGEUP		= 33;
	this.PAGEDOWN	= 34;
	
	this.SPACE		= 32;
	
	this.LEFT		= 37;
	this.UP			= 38;
	this.RIGHT		= 39;
	this.DOWN		= 40;
	
	
	this.ENTER		= 13;
	this.ESC		= 27;

	this.SHIFT		= 16;
	this.CTRL		= 17;
	this.ALT		= 18;

	this.C			= 67;
	this.X			= 88;
	this.V			= 86;
	
	this.T			= 84;
	this.S			= 83;
	this.R			= 82;

	
	
	var pressed = {};
	
	var modifiers = 0;
	
	var modLookup = {};
	

	this.isDown = function(keycode) {
		return pressed[keycode];
	};

	this.isUp = function(keycode) {
		return !pressed[keycode];
	};
	
	
	 
	
	var pressRegister = {};
	this.registerPress = function(key, modifiers, handler){
		register(pressRegister, key, modifiers, handler);
	};
	
	var releaseRegister = {};
	this.registerRelease = function(key, modifiers, handler){
		register(releaseRegister, key, modifiers, handler);
	};
	
	var holdRegister = {};
	this.registerHold = function(key, modifiers, handler){
		register(holdRegister, key, modifiers, handler);
	};
	
	function register(reg, key, modifiers, handler){
		
		var modifierMask = 0;
		
		if(modifiers != null)
		{
			for(var i = 0; i < modifiers.length; i++)
			{
				if(!modLookup[modifiers[i]])
				{
					var length = Object.keys(modLookup).length;
					modLookup[modifiers[i]] = 1 << length;
				}
				
				modifierMask |= modLookup[modifiers[i]];
			}
	
			reg[key] = {
				handler: handler,
				modifiers: modifierMask
			};
		}
	}
	
	
	function onKeydown (keycode) {
		
		// if the key is already pressed, don't trigger another press event, but DO trigger a keyhold event
		if(pressed[keycode]) {
			triggerEvent(holdRegister[keycode]);
			return;
		}
		
		// add key to the list of currently pressed keys
		pressed[keycode] = true;
		
		// if the key is on the list of modifiers, add it to the modifier bit mask
		if(modLookup.hasOwnProperty(keycode))
		{
			modifiers |= modLookup[keycode];
		}
		
		triggerEvent(pressRegister[keycode]);
	}

	
	function onKeyup(keycode) {
	
		// remove key from the list of currently pressed keys
		delete pressed[keycode];
		
		// if the key is on the list of modifiers, remove it from the modifier bit mask
		if(modLookup.hasOwnProperty(keycode))
		{
			modifiers &= ~(modLookup[keycode]);
		}
		
		triggerEvent(releaseRegister[keycode]);
	}
	
	function triggerEvent(register)
	{
		// if the register is valid, and has a valid handler, and all the modifiers are active - call the handler
		if(register != null && register.handler != null && (register.modifiers & modifiers) == register.modifiers)
		{
			register.handler();
		}
	}
	
	window.addEventListener('keydown', function(event) { onKeydown(event.keyCode); }, false);
	window.addEventListener('keyup', function(event) { onKeyup(event.keyCode); }, false);
	
	

});