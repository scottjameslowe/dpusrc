app.service('dpuService', function(){
	
	var socket = io.connect('http://192.168.1.100');
	
	var buffer = generateBuffer();
	
	var updateHandlers = [];
	
	
	socket.on('respondCurrentBuffer', function(responseData){
		
		for(var i = 0; i < 64; i++){
			buffer[i] = responseData[i];
		}
		var c = updateHandlers.length;
		for(var i = 0; i < c; i++){
			updateHandlers[i]();
		}
	});
	
	function registerUpdateHandler(handler){
		updateHandlers.push(handler);
	}
	
	function refresh(){
		socket.emit('requestCurrentBuffer');
	}
	
	function draw(bufferData){
		socket.emit('draw', bufferData);
	}
	
	function generateBuffer(){
		var dat = [];
		for(var i = 0; i < 64; i++){
			dat[i] = 0x00;
		}
		return dat;
	}
	
	this.socket = socket;
	this.buffer = buffer;
	
	this.registerUpdateHandler = registerUpdateHandler;
	this.draw = draw;
	this.refresh = refresh;
	this.generateBuffer = generateBuffer;
});

app.factory('dpuBuffer', function(dpuService){

	var buffer = dpuService.generateBuffer();
	
	
	
	
	
	// public
	
	function draw(){
		dpuService.draw(buffer);
	}
	
	function refresh(){
		dpuService.refresh();
	}
	
	
	function setVoxel(x,y,z,a,bufferData){	
		
		if(bufferData == null){ bufferData = buffer; }
		
		x <<= 0;
		y <<= 0;
		z <<= 0;
		
		var i = toAddress(y,z);
		if(a)   { bufferData[i] |= (0x01<<x); }
		else    { bufferData[i] &= ~(0x01<<x); }
	}

	function getVoxel(x,y,z,bufferData){
		
		if(bufferData == null){ bufferData = buffer; }
		
		var i = toAddress(y,z);
		return ( (bufferData[i] & (0x01<<x)) != 0 );
	}

	function toggleVoxel(x,y,z,bufferData){
		
		if(bufferData == null){ bufferData = buffer; }
		
		var a = getVoxel(x,y,z,bufferData);
		setVoxel(x,y,z,!a,bufferData);
	}
	
	function copyVoxel(x,y,z,source,bufferData){
		
		if(bufferData == null){ bufferData = buffer; }
		
		var a = getVoxel(x,y,z,source);
		setVoxel(x,y,z,a,bufferData);
	}
	
	
	
	function fill(fillValue, bufferData){
	
		if(bufferData == null){ bufferData = buffer; }
		
		for(var i = 0; i < 64; i++){
			bufferData[i] = fillValue;
		}
	}
	
	function copy(source, destination){
		
		for(var i = 0; i < 64; i++){
			destination[i] = source[i];
		}
	}
	
	function copyTo(destination){
		
		copy(buffer, destination);
	}
	
	function copyFrom(source){
		
		copy(source, buffer);
	}
	
	function drawBounds(start, end, bufferData){
		
		if(bufferData == null){ bufferData = buffer; }
		
		loopBounds(start, end, function(x,y,z){
					
			var alpha = 0;
			
			alpha += (x == min.x || x == max.x)? 1 : 0;
			alpha += (y == min.y || y == max.y)? 1 : 0;
			alpha += (z == min.z || z == max.z)? 1 : 0;
			
			alpha = (alpha > 1);
			
			setVoxel(x, y, z, alpha, bufferData);
			
		});
	}
	
	function copyRegion(source, start, end, bufferData){
	
		if(bufferData == null){ bufferData = buffer; }
		
		loopBounds(start, end, function(x,y,z){
			
			copyVoxel(x,y,z,source,bufferData);
		});
	}
	
	function fillRegion(start, end, alpha, bufferData){
	
		if(bufferData == null){ bufferData = buffer; }
		
		loopBounds(start, end, function(x,y,z){
			
			setVoxel(x,y,z,alpha,bufferData);
		});
	}
	
	function moveRegion(start, end, vector, bufferData){
	
		if(bufferData == null){ bufferData = buffer; }
		
		var source = [];
		copy(bufferData, source);
		
		loopBounds(start, end, function(x,y,z){
			var nX = x+vector.x;
			var nY = y+vector.y;
			var nZ = z+vector.z;
			
			if(nX < 0 || nX > 7 || nY < 0 || nY > 7 || nZ < 0 || nZ > 7)
			{
				return;
			}
			
			var alpha = read(x,y,z,source);
			write(nX,nY,nZ,alpha,bufferData);
		});
	}
	
	
	// function overlay(
	
	// private
	
	function toAddress(y,z){
		return (y<<3) + z;
	}
	
	function loopBounds(start, end, handler){
		
		var bounds = normalizeBounds(start, end);
		var min = bounds.min;
		var max = bounds.max;
		
		for(var x = min.x; x <= max.x; x++)
		{
			if(x < 0 || x > 7) { continue; }
			for(var y = min.y; y <= max.y; y++)
			{
				if(y < 0 || y > 7) { continue; }
				for(var z = min.z; z <= max.z; z++)
				{
					if(z < 0 || z > 7) { continue; }
					handler(x,y,z);
				}
			}
		}
		
	}
	
	function normalizeBounds(a, b){
		var min = {};
		var max = {};
		
		if(a.x < b.x){
			min.x = a.x;
			max.x = b.x;
		} else {
			min.x = b.x;
			max.x = a.x;
		}
		
		
		if(a.y < b.y){
			min.y = a.y;
			max.y = b.y;
		} else {
			min.y = b.y;
			max.y = a.y;
		}
		
		
		if(a.z < b.z){
			min.z = a.z;
			max.z = b.z;
		} else {
			min.z = b.z;
			max.z = a.z;
		}
		
		return { min:min, max:max }
	}
	
	function clone(){
		
		var result = generateBuffer();
		copyTo(result);
		return result;
	}
	

	
	function generateBuffer()
	{
		var dpuBuffer = {};
		
		
		dpuBuffer.draw = draw;
		dpuBuffer.refresh = refresh;
		
		dpuBuffer.setVoxel = setVoxel;
		dpuBuffer.getVoxel = getVoxel;
		dpuBuffer.toggleVoxel = toggleVoxel;
		dpuBuffer.copyVoxel = copyVoxel;
		
		dpuBuffer.fill = fill;
		dpuBuffer.copyTo = copyTo;
		dpuBuffer.copyFrom = copyFrom;
		dpuBuffer.drawBounds = drawBounds;
		dpuBuffer.copyRegion = copyRegion;
		dpuBuffer.fillRegion = fillRegion;
		dpuBuffer.moveRegion = moveRegion;
		
		
		dpuBuffer.spawn = generateBuffer;
		dpuBuffer.clone = clone; 
		
		
		dpuService.registerUpdateHandler(function(bufferData){
			buffer = bufferData;
		});
		
		return dpuBuffer;
		
	}
	
	
		
	return generateBuffer();	
		
		
	
	
	// drawling functions
	




// function copyBuffer(dpuBuffer buffer)
// {
	// byte y = 7;
	// byte z = 7;

	// do
	// {
		// do
		// {
			// drawBuffer[y][z] = buffer[y][z];
		// }
		// while(z--);
	// }
	// while(y--);
// }

// void compositeBuffer(dpuBuffer buffer)
// {
	// byte y = 7;
	// byte z = 7;

	// do
	// {
		// do
		// {
			// drawBuffer[y][z] |= buffer[y][z];
		// }
		// while(z--);
	// }
	// while(y--);
// }

// void flipBuffer(dpuBuffer buffer)
// {
	// byte y = 7;
	// byte z = 7;

	// do
	// {
		// do
		// {
			// drawBuffer[y][z] ^= buffer[y][z];
		// }
		// while(z--);
	// }
	// while(y--);
// }


// void setVoxel(int x, int y, int z)
// {
	// drawBuffer[y][z] |= (0x01<<x);
// }

// void clearVoxel(int x, int y, int z)
// {
	// drawBuffer[y][z] &= ~(0x01<<x);
// }

// void writeVoxel(int x, int y, int z, int value)
// {
	// if (value)	setVoxel(x,y,z);
	// else		clearVoxel(x,y,z);
// }

// byte getVoxel(int x, int y, int z)
// {
	// if (drawBuffer[y][z] & (1 << x)) return 0x01;
	// else return 0x00;
// }

// void flipVoxel(int x, int y, int z)
// {
	// drawBuffer[y][z] ^= (1 << x);
// }

// void setLine(int x1, int y1, int z1, int x2, int y2, int z2)
// {
	// float xy;	// how many voxels do we move on the y axis for each step on the x axis
	// float xz;	// how many voxels do we move on the y axis for each step on the x axis 
	// unsigned char x,y,z;
	// unsigned char lasty,lastz;

	////We always want to draw the line from x=0 to x=7.
	////If x1 is bigget than x2, we need to flip all the values.
	// if (x1>x2)
	// {
		// int tmp;
		// tmp = x2; x2 = x1; x1 = tmp;
		// tmp = y2; y2 = y1; y1 = tmp;
		// tmp = z2; z2 = z1; z1 = tmp;
	// }

	
	// if (y1>y2)
	// {
		// xy = (float)(y1-y2)/(float)(x2-x1);
		// lasty = y2;
	// } else
	// {
		// xy = (float)(y2-y1)/(float)(x2-x1);
		// lasty = y1;
	// }

	// if (z1>z2)
	// {
		// xz = (float)(z1-z2)/(float)(x2-x1);
		// lastz = z2;
	// } else
	// {
		// xz = (float)(z2-z1)/(float)(x2-x1);
		// lastz = z1;
	// }



	// for (x = x1; x<=x2;x++)
	// {
		// y = (xy*(x-x1))+y1;
		// z = (xz*(x-x1))+z1;
		// setVoxel(x,y,z);
	// }
	
// }

	// void setPlaneZ (int z)
	// {
		// if (z < 0 || z > 7) return;

		// for (byte y=0; y<8; y++)
			// drawBuffer[y][z] = 0xff;
	// }


	// void clearPlaneZ (int z)
	// {
		// if (z < 0 || z > 7) return;

		// for (byte y=0; y<8; y++)
			// drawBuffer[y][z] = 0x00;
	// }

	// void setPlaneX (int x)
	// {
		// if (x < 0 || x > 7) return;

		// for (byte z=0; z<8; z++)
		// {
			// for (byte y=0; y<8; y++)
			// {
				// drawBuffer[y][z] |= (1 << x);
			// }
		// }
	// }

	// void clearPlaneX (int x)
	// {
		// if (x < 0 || x > 7) return;

		// for (byte z=0; z<8; z++)
		// {
			// for (byte y=0; y<8; y++)
			// {
				// drawBuffer[y][z] &= ~(1 << x);
			// }
		// }
	// }

	// void setPlaneY (int y)
	// {
		// if (y < 0 || y > 7) return;

		// for (byte z=0; z<8; z++)
			// drawBuffer[y][z] = 0xff;
	// }

	// void clearPlaneY (int y)
	// {
		// if (y < 0 || y > 7) return;

		// for (byte z=0; z<8; z++)
			// drawBuffer[y][z] = 0x00;
	// }


	// void fill (byte pattern)
	// {
		// for (byte y=0;y<8;y++)
		// {
			// for (byte z=0;z<8;z++)
			// {
				// drawBuffer[y][z] = pattern;
			// }
		// }
	// }


	// void copyCell(dpuCell origin, dpuCell destination)
	// {
		

	// }
	
});