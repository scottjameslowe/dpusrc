var app = angular.module('dpuApp', []);

app.controller('dpuController', function($scope)
{
	
	$scope.isActive = false;
	
	
	$scope.updateIsActive = function()
	{
		dpu.setActive( $scope.isActive );
	};

	// $scope.isEnabled = dpu.getEnabled;

	// $scope.refreshActive = function()
	// {
	//   $scope.active = dpu.getActive();
	// }
  
	$scope.address_x = 0;
	$scope.address_y = 0;
	$scope.address_z = 0;
 
	$scope.arr = [1,1,1,1,1,1,1,1];


	$scope.isAddress_z = function(i){
	return ($scope.address_z == i);
	};

	$scope.selectZ = function(i){
		$scope.address_z = i;
	};
  
	$scope.getVoxel = function(x, y)
	{
	  return dpu.getVoxel(x, 7-y, 7-$scope.address_z);
	};

	$scope.toggleVoxel = function(x, y)
	{
	  dpu.toggleVoxel(x, 7-y, 7-$scope.address_z);
	};

// Leap motion stuff


  var init = function(){
        

    var fingers = {};
    var spheres = {};

    Leap.loop(function(frame) {

      var fingerIds = {};
      var handIds = {};

      for (var index = 0; index < frame.pointables.length; index++) {

        var pointable = frame.pointables[index];
        var finger = fingers[pointable.id];

        var pos = pointable.tipPosition;
        var dir = pointable.direction;

        // var origin = new THREE.Vector3(pos[0], pos[1], pos[2]);
        // var direction = new THREE.Vector3(dir[0], dir[1], dir[2]);

        

        var x = ((pos[0] + 256) / 64) >> 0;
        var y = ((pos[1] + 256) / 64) >> 0;
        var z = ((pos[2] + 256) / 64) >> 0;

        if(x < 0) x = 0;
        if(x > 7) x = 7;

        if(y < 0) y = 0;
        if(y > 7) y = 7;

        if(z < 0) z = 0;
        if(z > 7) z = 7;

        console.log(x, y, z);

        $scope.addressX = x;
        $scope.addressY = y;
        $scope.addressZ = z;

        $scope.voxel[x][y][z] = true;//! $scope.voxel[x][y][z];

        $scope.isDirty = true;

        if (!finger) {
          // finger = new THREE.ArrowHelper(origin, direction, 40, Math.random() * 0xffffff);
          fingers[pointable.id] = finger;
          // scene.add(finger);
        }

        // finger.position = origin;
        // finger.setDirection(direction);

        fingerIds[pointable.id] = true;
      }

      for (fingerId in fingers) {
        if (!fingerIds[fingerId]) {
        // scene.remove(fingers[fingerId]);
        delete fingers[fingerId];
        }
      }
    });

  };
//   init();




// setInterval(reDraw, 200);



function reDraw()
{
  if($scope.isDirty)
  {
    $scope.isDirty = false;
    $scope.submitChange();
  }
}




















});


