


#include <node.h>
#include <v8.h>
#include <dpuIO.h>

using namespace v8;

// Public functions

Handle<Value> setup(const Arguments& args)
{
    HandleScope scope;

    int initSuccess = openDpuIO();

    return scope.Close(String::New(((initSuccess != 0)? "Setup successful." : "Setup failed.")));
}


Handle<Value> drawVoxel(const Arguments& args)
{
	HandleScope scope;

	int x = args[0]->Uint32Value();
	int y = args[1]->Uint32Value();
	int z = args[2]->Uint32Value();
	bool a = args[3]->ToBoolean()->Value();

	if(x < 0) { x = 0; }
	if(x > 7) { x = 7; }

	if(y < 0) { y = 0; }
	if(y > 7) { y = 7; }

	if(z < 0) { z = 0; }
	if(z > 7) { z = 7; }

	setVoxel(x, y, z, a);
  
	//char buffer[512];
	//sprintf(buffer, "Draw Voxel Success.\n x: %d, y: %d, z: %d \n address: %d, value: %d", x, y, z, address, (0x01<<x));
	
	return scope.Close(String::New("Draw Voxel Success.\n"));
}


Handle<Value> drawFrame(const Arguments& args)
{
	HandleScope scope;
	
	Handle<Array> buffer = Handle<Array>::Cast(args[0]);
	
	setAddress(0);
	
	for(char i = 0; i < buffer->Length(); i++)
	{
		writeAndIncrement( buffer->Get(i)->Uint32Value() );
	}
	
	return scope.Close(String::New("Draw Success.\n"));
}


void init(Handle<Object> exports)
{
	exports->Set(String::NewSymbol("setup"), FunctionTemplate::New(setup)->GetFunction());
	exports->Set(String::NewSymbol("drawVoxel"), FunctionTemplate::New(drawVoxel)->GetFunction());
	exports->Set(String::NewSymbol("drawFrame"), FunctionTemplate::New(drawFrame)->GetFunction());
}

NODE_MODULE(dpuInterface, init)
