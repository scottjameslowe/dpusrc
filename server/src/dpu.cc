
/*

	[Usage]
	
	dpu 	-setPin [pin number]		pull specified pin high
			-clrPin [pin index]			pull specified pin low
			
			-setTrg						pull the trigger pin high (must be high before set low)
			-clrTrg						pull the trigger pin low (triggers operation)
			-setOp [value]				set the pins on the op bus
			-setBus [value]				set the pins on the data bus
			
			-write [adr] [value]		write the line value at address
			-writeInc [value]			write the line value at the "current address" and then increment the "current address"
			-setMsk [adr] [value]		turn on bits set to "1" in the line value at address
			-clrMsk [adr] [value]		turn off bits set to "1" in the line value at address
			-adr [adr]					set the "current address"
			-lc [count]					set the number of layers the dpu hardware should use
			-rc [count]					set the number of registers the dpu hardware should use
			-set [x] [y] [z]			turn on the voxel at the x,y,z location
			-clr [x] [y] [z]			turn off the voxel at the x,y,z location

*/

#include "dpuIO.h"


int main(int argc, char **argv)
{
	printf ("dpu command\n");
	
	if(openDpuIO() != 0) 
	{
		printf ("IO init error\n");
		closeDpuIO();
		return 1;
	}
	
	for( int i = 1; i < argc; i++)
	{
	
	
		if(strcmp (argv[i], "-setPin") == 0)
		{
			writePin( atoi(argv[++i]), true );
		}
		else if (strcmp (argv[i], "-clrPin") == 0)
		{
			writePin( atoi(argv[++i]), false );
		}
		
		else if (strcmp (argv[i], "-setTrg") == 0)
		{
			setTrigger( true );
		}
		else if (strcmp (argv[i], "-clrTrg") == 0)
		{
			setTrigger( false );
		}
		else if (strcmp (argv[i], "-setOp") == 0)
		{
			setOp( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-setBus") == 0)
		{
			setBus( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-write") == 0)
		{ 
			printf ("write\n");
			setAddress( atoi(argv[++i]) );
			write( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-writeInc") == 0)
		{
			writeAndIncrement( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-setMsk") == 0)
		{
			setAddress( atoi(argv[++i]) );
			setMask( atoi(argv[++i]));
		}
		else if (strcmp (argv[i], "-clrMsk") == 0)
		{
			setAddress( atoi(argv[++i]) );
			clearMask( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-adr") == 0)
		{
			setAddress( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-lc") == 0)
		{
			setLayerCount( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-rc") == 0)
		{
			setRegisterCount( atoi(argv[++i]) );
		}
		else if (strcmp (argv[i], "-set") == 0)
		{
			setVoxel( atoi(argv[++i]), atoi(argv[++i]), atoi(argv[++i]), true );
		}
		else if (strcmp (argv[i], "-clr") == 0)
		{
			setVoxel( atoi(argv[++i]), atoi(argv[++i]), atoi(argv[++i]), false );
		}
		else if (strcmp (argv[i], "-fill") == 0)
		{
			int fillVal = atoi(argv[++i]);

			setAddress( 0 );
			for(int c = 0; c < 64; c++)
			{
				writeAndIncrement( fillVal );
			}
		}
		else if (strcmp (argv[i], "-help") == 0)
		{
			printf ( "    -setPin [pin number]     pull specified pin high\n" );
			printf ( "    -clrPin [pin index]      pull specified pin low\n" );
			
			printf ( "    -setTrg                  pull the trigger pin high (must be high before set low)\n" );
			printf ( "    -clrTrg                  pull the trigger pin low (triggers operation)\n" );
			printf ( "    -setOp [value]           set the pins on the op bus\n" );
			printf ( "    -setBus [value]          set the pins on the data bus\n" );
			
			printf ( "    -write [adr] [value]     write the line value at address\n" );
			printf ( "    -writeInc [value]        write the line value at the \"current address\" and then increment the \"current address\"\n" );
			printf ( "    -setMsk [adr] [value]    turn on bits set to \"1\" in the line value at address\n" );
			printf ( "    -clrMsk [adr] [value]    turn off bits set to \"1\" in the line value at address\n" );
			printf ( "    -adr [adr]               set the \"current address\"\n" );
			printf ( "    -lc [count]              set the number of layers the dpu hardware should use\n" );
			printf ( "    -rc [count]              set the number of registers the dpu hardware should use\n" );
			printf ( "    -set [x] [y] [z]         turn on the voxel at the x,y,z location\n" );
			printf ( "    -clr [x] [y] [z]         turn off the voxel at the x,y,z location\n" );

			printf ( "    -fill [value]            fill each line with value\n" );
		}
		else
		{
			printf ("Unknown argument\n");
			return 1;
		}
		
		
	}
	
	closeDpuIO();
	
	return 0;
}
