/*
*	Author: Scott Lowe
*	Email: scottjameslowe@gmail.com
*	Description: Serial communication module for the DPU board firmware.
*/

#include <dpu_driver.h>
#include <dpu_serial.h>
#include <dpu_graphics.h>

#include <include/avr/io.h>
#include <include/avr/interrupt.h>


static const dpuCommand commands[] =
{
    // { sizeof(dpuFrame), &dpu_graphics_drawFrame, NULL }
};


static byte dataBuffer[RX_BUFFER_SIZE];
static uint16_t bufferWriteIndex = 0;
// static uint16_t bufferReadIndex = 0;
// static uint16_t commandCounter = 0;
static byte cycleCount = 0;


// static dpuCommand * currentCommand;

ISR(USART0_RX_vect) 
{
    dataBuffer[bufferWriteIndex] = UDR0;
    if(++bufferWriteIndex >= RX_BUFFER_SIZE)
    {
        bufferWriteIndex = 0;
        cycleCount++;
    }
}

void refreshSerial(void)
{
    // while(bufferReadIndex < bufferWriteIndex)
    // {
    //     volatile byte * currentByte = &dataBuffer[bufferReadIndex];

    //     if(++bufferReadIndex >= RX_BUFFER_SIZE)
    //     {
    //         bufferReadIndex = 0;
    //         cycleCount--;
    //     }

    //     if(currentCommand == NULL)
    //     {
    //         currentCommand = &commands[*currentByte];
    //         commandCounter = 0;
    //     }
    //     else
    //     {
    //         if(currentCommand->buffer == NULL)
    //         {
    //             currentCommand->buffer = (void*)currentByte;
    //         }

    //         if(++commandCounter >= currentCommand->length)
    //         {
    //             commandEvent result = currentCommand->handler(currentCommand->buffer);
    //             currentCommand = NULL;
    //             dpu_serial_transmit(result);
    //         }
    //     }
    // }
    
}



void handleBufferOverflow(void)
{
    dpu_serial_transmit(COMMAND_EVENT_ERROR_BUFFER_OVERFLOW);
}

void dpu_serial_transmit(byte val)
{
    // Wait for empty transmit buffer
    while ( !(UCSR0A & (1<<UDRE0)) )
    ;
    // Put data into buffer, sends the data
    UDR0 = val;
}

void initSerial(void)
{
    UBRR0H = (unsigned char)(UBRRVAL>>8);		// Set baud rate - High byte
    UBRR0L = (unsigned char) UBRRVAL;			// Set baud rate - Low byte

    UCSR0A = (0<<U2X0);							// Asynchronous normal speed

    //Enable Transmitter and Receiver and Interrupt on receive complete
    UCSR0B = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0);

    //page 186 Set asynchronous mode,no parity, 1 stop bit, 8 bit size
    UCSR0C= (0<<UMSEL00)| (0<<UMSEL01)|             //Async
            (0<<UPM00)  | (0<<UPM01)  |             //Parity None
            (0<<USBS0)  |                           //Stop bits 1
            (0<<UCSZ02) | (1<<UCSZ01) |(1<<UCSZ00); //8 Bits
			
    //enable interrupts
    sei();
}

