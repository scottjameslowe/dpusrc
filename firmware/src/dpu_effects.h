/*
*	Author: Scott Lowe
*	Email: scottjameslowe@gmail.com
*	Description: Core code-set for the DPU board firmware
*	Comments: 
*/


#ifndef DPU_EFFECTS_H
#define DPU_EFFECTS_H


#ifdef __cplusplus
extern "C"{
#endif

typedef void (*dpuEffect)(void);

void serialOnOff(void);
void planeSwipe(void);
void planeSwipe2(void);
void planeSwipe3(void); 

void planeSwap(void);

void rain(void);
void snake(void);
void randomNoise(void);
void volumeNoise(int delayVal);
void randomCubes(void);
void effect_test (void);
void effect_stringfly2(char * str);

void drawText(void);

void openCenter(void);




void setTo(int x, int y, int z);
void moveTo(int x, int y, int z, int d);
void loopLine(int x, int y, int z, int xM, int yM, int zM, int d);

#ifdef __cplusplus
}
#endif
#endif // DPU_EFFECT_H