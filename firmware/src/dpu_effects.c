/*
*	Author: Scott Lowe
*	Email: scottjameslowe@gmail.com
*	Description: Animations for the dpu.
*/

#include <dpu_driver.h>
#include <dpu_graphics.h>
#include <font.h>
#include <math.h>
#include <stdlib.h> 

void serialOnOff(void)
{
	for(byte y = 0; y < 8; y++)
	{
		for(byte z = 0; z < 8; z++)
		{
			for(byte x = 0; x < 8; x++)
			{
				setVoxel(x,y,z);
				delay(20);
			}
		}
	}

	for(byte y = 0; y < 8; y++)
	{
		for(byte z = 0; z < 8; z++)
		{
			for(byte x = 0; x < 8; x++)
			{
				clearVoxel(x,y,z);
				delay(20);
			}
		}
	}

}

void rain(void)
{
	int x = randomRange(0, 7);
	int z = randomRange(0, 7);
	int y = 7;
	int length = 3;
	int total = 20;

	dpuVoxel drops[10];
	for(int i = 0; i < 10; i++)
	{
		drops[i].x = randomRange(0, 7);
		drops[i].z = randomRange(0, 7);
		drops[i].y = randomRange(8, 16);
	}

	do
	{
		for(int i = 0; i < 10; i++)
		{
			clearVoxel(drops[i].x, drops[i].y+(length-1), drops[i].z);
			drops[i].y--;
			setVoxel(drops[i].x, drops[i].y, drops[i].z);
		}
		delay(60);
	}
	while(total--);

}

void planeSwipe(void)
{
	int l = 0;

	for(int c = 0; c < 3; c++)
	{
		for(int i = 0; i < 8; i++)
		{
			switch(c)
			{
				case 0:
					if(i > 0) clearPlaneZ(l);
					setPlaneZ(i);
					break;
				case 1:
					if(i > 0) clearPlaneX(l);
					setPlaneX(i);
					break;
				case 2:
					if(i > 0) clearPlaneY(l);
					setPlaneY(i);
					break;
			}


			l = i;
			delay(200);
		}
	}
}
void planeSwipe2(void)
{
	int l = 0;
	int cn = 3;
	int in = 8;
	int dir = 1;

	for(int c = 0; c != cn; c+=dir)
	{
		for(int i = 0; i != in; i+=dir)
		{
			switch(c)
			{
				case 0:
					clearPlaneZ(l);
					setPlaneZ(i);
					break;
				case 1:
					clearPlaneX(l);
					setPlaneX(i);
					break;
				case 2:
					clearPlaneY(l);
					setPlaneY(i);
					break;
			}
			
			l = i;
			delay(50);
		}
	}
}

void planeSwipe3(void)
{
	int l = 0;
	int lk = -3;

	for(int c = 0; c < 3; c++)
	{
		for(int i = 0; i < 8; i++)
		{
			switch(c)
			{
				case 0:
					if(i > 0) clearPlaneZ(l);
					if(i > 3) clearPlaneZ(lk);
					setPlaneZ(i);
					setPlaneZ(i-3);
					break;
				case 1:
					if(i > 0) clearPlaneX(l);
					if(i > 3) clearPlaneX(lk);
					setPlaneX(i);
					setPlaneX(i-3);
					break;
				case 2:
					if(i > 0) clearPlaneY(l);
					if(i > 3) clearPlaneY(lk);
					setPlaneY(i);
					setPlaneY(i-3);
					break;
			}

			lk = i-3;
			l = i;
			delay(200);
		}
	}
}

void planeSwap(void)
{

	for(int c = 0; c < 3; c++)
	{
		int flag[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

		int _x = 0;
		int _z = 0;

		for(int i = 0; i < 64; i++) // until all x, z values are set
		{
			// loop through random x, z values until reaching a combination that has NOT been set
			do
			{
				int _x = randomRange(0,7);
				int _z = randomRange(0,7);
			}
			while (flag[_x] & (1<<_z) != 0);

			flag[_x] |= (1<<_z);

			int cnt = 8;
			while(cnt--)
			{
				int cn = cnt-1;
				switch(c)
				{
					case 0:
						setVoxel(cn, _x, _z);
						clearVoxel(cn+1, _x, _z);
						break;
					case 1:
						setVoxel(_x, cn, _z);
						clearVoxel(_x, cn+1, _z);
						break;
					case 2:
						setVoxel(_x, _z, cn);
						clearVoxel(_x, _z, cn+1);
						break;
				}

				delay(200);
			}
		}
	}
}

void volumeNoise(int delayVal)
{
	int y;
	int cnt = 50;

	int val[8][8];

	for(int i = 0; i < 8; i++)
		for(int j = 0; j < 8; j++)
			val[i][j] = 0;

	while(cnt--)
	{
		fill(0);

		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				y = val[i][j] + randomRange(0,2) - 1;
				if (y < 0) y = 0;
				if (y > 7) y = 7;
				val[i][j] = y;

				for(int k = 0; k < y; k++)
				{
					setVoxel(i, k, j);
				}
			}
		}

		delay(delayVal);
	}


}

void randomNoise(void)
{
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			for(int k = 0; k < 8; k++)
			{
				int rnd = randomRange(0, 1);
				writeVoxel(i, j, k, rnd);
			}
		}
	}

	delay(60);
}

void randomCubes(void)
{
	int x = 0;
	int _x = 0;
	int y = 0;
	while(1)
	{
		x = 4 + randomRange(-4, 8);
		y = x + randomRange(0, 8-x);

		for(int i = x; i < y; i++)
			for(int j = x; j < y; j++)
				for(int k = x; k < y; k++)
					setVoxel(i, j, k);
		delay(300);

		for(int i = x; i < y; i++)
			for(int j = x; j < y; j++)
				for(int k = x; k < y; k++)
					clearVoxel(i, j, k);
	}

}

// -3, 3
// 0, 1
// 0, 5
// 0, 2
// 4, 7

int randomRange(int lower, int upper)
{
	int r = lower;
	if(lower < 0)
	{
	 // 	upper -= lower;
	 // 	if(lower > 0)
		// {
		// 	upper -= lower;	
		// 	r += lower;
		// }
 	}
 	else
 	{
 		upper -= lower;
		r = (int)( rand() / (RAND_MAX / (upper+1)) );		// 37000 / 2 = 
		r += lower;
	}
	return r;
}

void snake(void)
{
	dpuVoxel snake = {0,0,0};
	
	int axis = 1;
	int lastaxis = 1;
	int dir = 1;
	int lcnt = 0;
	int trvldist = 1;
	int nextLoc;

	while(1)
	{

		switch(axis)
		{
			case 0:
				nextLoc = snake.x + dir;
				break;
			case 1:
				nextLoc = snake.y + dir;
				break;
			case 2:
				nextLoc = snake.z + dir;
				break;
		}

		if(nextLoc > 7)
		{
			lastaxis = axis;
			while(lastaxis == axis)
			{
				axis = randomRange(0, 3);
				
			}
		}

		setVoxel(snake.x, snake.y, snake.z);
		delay(400);


		if(++lcnt > trvldist)
		{

			lcnt = 0;

			if(axis != lastaxis)
			{
				// int dirFlip = randomRange(0,1);
				// if(dirFlip)
					dir = -dir;
			}


			lastaxis = axis;
			axis = randomRange(0, 3);
		}
		// clearVoxel(0, 0, 0);
		// clearVoxel(0, 0, 1);
		// clearVoxel(0, 0, 2);
		// clearVoxel(0, 0, 3);
		// clearVoxel(0, 0, 4);
		// clearVoxel(0, 0, 5);
		// clearVoxel(0, 0, 6);
		// clearVoxel(0, 0, 7);

		// setVoxel(0, 0, axis);

	}




}

void effect_test (void)
{

	int x,y,i;

	for (i=0;i<1000;i++)
	{
		x = sin(i/8)*2+3.5;
		y = cos(i/8)*2+3.5;

		setVoxel(x,y,1);
		setVoxel(x,y,1);
		delay(5);
		fill(0x00);
	}
	delay(200);
}

void drawText(void)
{
	effect_stringfly2("A.I. SOLUTIONS    TEST WHAT YOU FLY   FLY WHAT YOU TEST");
}

//void effext_stringfly2 (char *str, char axis, char mirror, char direction, int delay, int space)
void effect_stringfly2(char * str)
{
	int x,y,i,ii;
	int state;
	
	unsigned char chr[5];
	
	while (*str)
	{
		font_getchar(*str++, chr);
		
		for (x = 0; x < 5; x++)
		{
			for (y = 0; y < 8; y++)
			{
				if ((chr[x] & (0x80>>y)))
				{
					setVoxel(x+2,y,7);
				}
			}
		}
		
		for (ii = 0; ii<6; ii++)
		{
			delay(150);
			for (i = 0; i < 7; i++)
			{
				for (x = 0; x < 8; x++)
				{
					for (y = 0; y < 8; y++)
					{
						state = getVoxel(x,y,i+1);
						writeVoxel(x,y,i,state);
					}
				}
			}
			for (x = 0; x < 8; x++)
			{
				for (y = 0; y < 8; y++)
				{
					clearVoxel(x,y,7);
				}
			}
		}
	}
	for (ii = 0; ii<8; ii++)
	{
		delay(150);
		for (i = 0; i < 7; i++)
		{
			for (x = 0; x < 8; x++)
			{
				for (y = 0; y < 8; y++)
				{
					state = getVoxel(x,y,i+1);
					writeVoxel(x,y,i,state);
				}
			}
		}

		for (x = 0; x < 8; x++)
		{
			for (y = 0; y < 8; y++)
			{
				clearVoxel(x,y,7);
			}
		}
	}
	
}

void openCenter(void)
{
	int i = 0;
	int j = 0;
	int k = 0;

	for(j = 0; j < 8; j++)
	{
		// for(i = 0; i < 8; i++)
		// {
		// 	moveTo(k,j,i,20);	// k = 6
		// 	k = 7 - i;
		// 	moveTo(i,j,k,20);
		// 	moveTo(k,j,k,20);	// i = 1
		// 	moveTo(k,j,i,20);
		// }

		setTo(0,j,0);
		moveTo(0,j,7,50);
		moveTo(7,j,7,50);
		moveTo(7,j,0,50);

		moveTo(1,j,0,50);
		moveTo(1,j,6,50);
		moveTo(6,j,6,50);
		moveTo(6,j,1,50);

		moveTo(2,j,1,50);
		moveTo(2,j,5,50);
		moveTo(5,j,5,50);
		moveTo(5,j,2,50);

		moveTo(3,j,2,50);
		moveTo(3,j,4,50);
		moveTo(4,j,4,50);
		moveTo(4,j,3,50);
	}

	fill(0x00);
}

int _x = 0;
int _y = 0;
int _z = 0;

void setTo(int x, int y, int z)
{
	_x = x;
	_y = y;
	_z = z;
}

void moveTo(int x, int y, int z, int d)
{
	loopLine(_x, _y, _z, x, y, z, d);
	setTo(x,y,z);
}

void loopLine(int x, int y, int z, int xM, int yM, int zM, int d)
{
	int s;
	int mx = 1;
	int my = 1;
	int mz = 1;

	if(xM < x)
	{
		s = x;
		x = xM;
		xM = s;
		// mx = -1;
	}

	if(yM < y)
	{
		s = y;
		y = yM;
		yM = s;
		// my = -1;
	}

	if(zM < z)
	{
		s = z;
		z = zM;
		zM = s;
		// mz = -1;
	}

	while(1)
	{
		setVoxel(x,y,z);
		if(d > 0) delay(d);

		if(x < xM) x+=mx;
		else if(y < yM) y+=my;
		else if(z < zM) z+=mz;
		else break;
	}
}



