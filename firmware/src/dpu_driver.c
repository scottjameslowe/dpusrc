#include <dpu_driver.h>

static volatile dpuBuffer drawBuffer;

static volatile byte layerIndex = 0;
static volatile byte bufferIndex = 0;

static volatile byte op_address = 0;

// static volatile uint16 counter = 0;
// static volatile uint16 compare = 16;

// update loop
void step(void)
{
	// --- WRITE CHANNELS ---------------------------------------------------------------------------

	// make sure the store clock is ready for pulse
	CLEAR(CHANNEL, STORE_CLOCK);

	const byte bufferIndexLimit = bufferIndex + REGISTER_COUNT;
	do
	{
		byte selectBit = REGISTER_SIZE;
		byte selectMask = (1 << (REGISTER_SIZE - 1));
		byte dat = drawBuffer[bufferIndex];

		do
		{
			// make sure the shift clock is ready for pulse
			CLEAR(CHANNEL, SHIFT_CLOCK);

			if( dat & selectMask )	SET(CHANNEL, DATA_WRITE);
			else					CLEAR(CHANNEL, DATA_WRITE);

			// pulse the shift clock to push data write pin to registers
			SET(CHANNEL, SHIFT_CLOCK);

			dat <<= 1;

		} while( --selectBit );

	} while (++bufferIndex < bufferIndexLimit);

	// pulse the store clock to instruct registers to set pin output
	SET(CHANNEL, STORE_CLOCK);

	
	// --- SET LAYER ---------------------------------------------------------------------------

	// make sure shift register is empty so only a single bit is selected
	// pulse shift reset to clear the shift register
	CLEAR(LAYER, SHIFT_RESET);
	SET(LAYER, SHIFT_RESET);

	// pulse the store clock to make sure two or more pins are never enabled simultaniously
	CLEAR(LAYER, STORE_CLOCK);
	SET(LAYER, STORE_CLOCK);

	// make sure the store clock is ready for pulse
	CLEAR(LAYER, STORE_CLOCK);
	
	// write the high pin
	CLEAR(LAYER, SHIFT_CLOCK);
	SET(LAYER, DATA_WRITE);
	SET(LAYER, SHIFT_CLOCK);

	// set the data pin low for remaining bits
	CLEAR(LAYER, DATA_WRITE);

	byte i = layerIndex;
	while(i--)
	{
		CLEAR(LAYER, SHIFT_CLOCK);
		SET(LAYER, SHIFT_CLOCK);
	}

	// store new data
	SET(LAYER, STORE_CLOCK);


	// --- RESET ---------------------------------------------------------------------------

	if(++layerIndex >= LAYER_COUNT)
	{
		layerIndex = 0;
		bufferIndex = 0;
	}
}


int main(void)
{
	// Setup port input/output
	DDRC = DATA_WRITE | STORE_RESET | STORE_CLOCK | SHIFT_CLOCK | SHIFT_RESET;
	DDRD = DATA_WRITE | STORE_RESET | STORE_CLOCK | SHIFT_CLOCK | SHIFT_RESET;

	// Set inital ports
	SET(LAYER, STORE_RESET);
	SET(LAYER, SHIFT_RESET);
	SET(CHANNEL, STORE_RESET);
	SET(CHANNEL, SHIFT_RESET);

    // Initialize the draw buffer
	byte i = 63;
	do { drawBuffer[i] = 0x0f; } while(i--);

	
	
	// --- Timer Setup ---

		// 1/FCPU * 64 * 16 = 69.4uS OR 14400 Hz

		// Configure timer 0 for CTC mode
//		TCCR0A |= (1 << WGM01); 

		// Set Prescaler
		// TCCR0B = (0<<CS00) | (0<<CS01) | (0<<CS02);	//No clock source (Timer/Counter stopped)
		// TCCR0B = (1<<CS00) | (0<<CS01) | (0<<CS02);	// FCPU
		// TCCR0B = (0<<CS00) | (1<<CS01) | (0<<CS02);	// FCPU/8
//		TCCR0B = (1<<CS00) | (1<<CS01) | (0<<CS02);	// FCPU/64
		// TCCR0B = (0<<CS00) | (0<<CS01) | (1<<CS02);	// FCPU/256
		// TCCR0B = (1<<CS00) | (0<<CS01) | (1<<CS02);	// FCPU/1024
		// TCCR0B = (0<<CS00) | (1<<CS01) | (1<<CS02);	// External clock source on T0 pin. Clock on falling edge.
		// TCCR0B = (1<<CS00) | (1<<CS01) | (1<<CS02);	// External clock source on T0 pin. Clock on rising edge.

		// Initialize Counter
//		TCNT0 = 0;

		// Set overflow compare to 128
//		OCR0A = 16;	

		// Set the number of steps per second
//		int ticks = 800;
		// disable timer
//		CLEAR(TIMER_MASK, INTERRUPT_BIT);

//		counter = 0;
//		compare = (uint16)(14400/ticks);

		
//		SET(TIMER_MASK, INTERRUPT_BIT);

		// Link the step function to the timer
		// handleTimerUpdate(step);

		// Start the timer
//		SET(TIMER_MASK, INTERRUPT_BIT);


    // ------------------------
	


	// --- Interface Setup ---

		// Setup port input/output
		CLEAR(DDRA, 0xff);									// Set data pins to input
		CLEAR(DDRB, (OP_TRIGGER | OPMSK_0 | OPMSK_1 | OPMSK_2));		// Set opcode pins to input

		// Disable pull-ups on input pins
		CLEAR(PORTA, 0xff);									// Disable pull-up resistors on all 'A' pins
		CLEAR(PORTB, (OP_TRIGGER | OPMSK_0 | OPMSK_1 | OPMSK_2));	// Disable pull-up resistors on 'B' opcode pins

		//EICRA = (0<<ISC21) | (0<<ISC20)					// Set interrupt to trigger on low
		EICRA = (1<<ISC21) | (0<<ISC20);					// Set interrupt to trigger on falling edge
		//EICRA = (1<<ISC21) | (1<<ISC20)					// Set interrupt to trigger on rising edge
		//EICRA = (0<<ISC21) | (1<<ISC20)					// Set interrupt to trigger on rising or falling edge

		EIMSK = (1<<INT2);									// Enable pin interrupt

    // ------------------------


	sei();													// Enable global interrupts

	while(1)
	{
		step();
	}
	
	return 0;
}

// --- Interface Trigger Handler ---

byte drawctr = 0;


ISR(INT2_vect)
{
	byte current_op = (PINB & OPMSK_0) | (PINB & OPMSK_1) | ((PINB & OPMSK_2)>>1);
	
	// byte i = 63;
	// do { drawBuffer[i] = 0x00; } while(i--);
	
	// drawBuffer[PINA] = 0xff;
	
	switch( current_op )
    {
        //// Do nothing
        case OP_NULL:
            break;

        //// Define the byte at the current address
        case OP_DEFINE:
            drawBuffer[op_address] = PINA;
            break;

        //// Define the byte at the current address, then increment the address (useful for entire buffer writes)
        case OP_DEFINC:
            drawBuffer[op_address] = PINA;
            op_address++;
            break;

        //// Set only 'set' bits in the byte at the current address
        case OP_SETMSK:
            SET(drawBuffer[op_address], PINA);
            break;

        //// Clear only 'set' bits in the byte at the current address
        case OP_CLRMSK:
            CLEAR(drawBuffer[op_address], PINA);
            break;

        //// Set the current address
        case OP_SETADR:
            op_address = PINA;
            break;

        //// Set the number of layers
        case OP_SETLRC:
            //// INCOMPLETE - PLEASE IMPLEMENT
            break;

        //// Set the number of registers
        case OP_SETRGC:
            //// INCOMPLETE - PLEASE IMPLEMENT
            break;
    }
}

// ISR(TIMER0_COMPA_vect)
// {
// 	counter++;
//
// 	if(counter >= compare)
// 	{
// 		counter = 0;
// 		//step();
// 	}
// }
