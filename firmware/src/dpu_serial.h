/*
*	Author: Scott Lowe
*	Email: scottjameslowe@gmail.com
*	Description: Serial communication module for the DPU board firmware.
*/

#ifndef DPU_SERIAL_H
#define DPU_SERIAL_H


#define RX_BUFFER_SIZE 4096
#define COMMAND_BUFFER_SIZE 512
#define BAUDRATE 230400
#define UBRRVAL ((F_CPU/(BAUDRATE*16UL)) -1)

#define COMMAND_EVENT_SUCCESS 0x00
#define COMMAND_EVENT_ERROR_GENERAL 0x01
#define COMMAND_EVENT_ERROR_BUFFER_OVERFLOW 0x02


#ifdef __cplusplus
extern "C"{
#endif

typedef unsigned char commandEvent;
typedef commandEvent (*serialHandler)(void*);
typedef struct
{
	uint16_t length;
	serialHandler handler;
	void * buffer;

} dpuCommand;

void refreshSerial(void);
void handleOverflowError(void);
void dpu_serial_transmit(byte);
void initSerial(void);




#ifdef __cplusplus
}
#endif
#endif // DPU_SERIAL_H