#ifndef DPU_DRIVER_H
#define DPU_DRIVER_H


#include <stdint.h>
#include <alloca.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


#define REGISTER_SIZE	8
#define REGISTER_COUNT	8
#define LAYER_COUNT		8

#define CHANNEL PORTC
#define LAYER   PORTD


// the dpu board was designed so the pins on port C and D both 
// matched identical input pins on both channel and layer register ic's
#define DATA_READ   (0x04) // A|B Pin2 (Data In)
#define DATA_WRITE  (0x08) // A|B Pin3 (Data Out)
#define STORE_RESET (0x10) // A|B Pin4 (Storage Reset)
#define STORE_CLOCK (0x20) // A|B Pin5 (Storage Clock)
#define SHIFT_CLOCK (0x40) // A|B Pin6 (Shift Clock)
#define SHIFT_RESET (0x80) // A|B Pin7 (Channel Shift Reset)

#define SET(port, bit) (port |= bit)
#define CLEAR(port, bit) (port &= ~bit)
#define READ(pin, mask) (pin & mask)


#define TIMER_MASK TIMSK0
#define INTERRUPT_BIT (1 << OCIE0A)


#define OPMSK_0		(0x01) // PORTB - Pin 0
#define OPMSK_1		(0x02) // PORTB - Pin 1
#define OP_TRIGGER	(0x04) // PORTB - Pin 2
#define OPMSK_2		(0x08) // PORTB - Pin 3

#define OP_NULL     (0x00)
#define OP_DEFINE   (0x01)
#define OP_DEFINC   (0x02)
#define OP_SETMSK   (0x03)
#define OP_CLRMSK   (0x04)
#define OP_SETADR   (0x05)
#define OP_SETLRC   (0x06)
#define OP_SETRGC   (0x07)






#ifdef __cplusplus
extern "C"{
#endif


typedef enum { false, true } bool;
typedef uint8_t byte;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;


typedef byte dpuBuffer[64];


void step(void);
int main(void);

#ifdef __cplusplus
}
#endif
#endif // DPU_DRIVER_H


/* dpu frame data structure

	index = registerNum + (registerCount*layerIdx);

				register
				0     1     2     3     4     5     6     7
layer	0		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		1		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		2		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		3		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		4		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		5		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		6		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		7		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff

*/







