/*
*	Author: Scott Lowe
*	Email: scottjameslowe@gmail.com
*	Description: Core code-set for the DPU board firmware
*	Comments: THIS CODE WILL NOT COMPILE
*/


#ifndef DPU_GRAPHICS_H
#define DPU_GRAPHICS_H

#include <dpu_serial.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef enum { set, composite, flip } drawType;
typedef struct { byte x; byte y; byte z; } dpuVoxel;
typedef struct { dpuVoxel start; dpuVoxel end; } dpuCell;
typedef struct { dpuCell origin; dpuCell destination; } dpuCellMap;

void duplicatBuffer(dpuBuffer origin, dpuBuffer destination);

void copyBuffer(dpuBuffer buffer);
void compositeBuffer(dpuBuffer buffer);
void flipBuffer(dpuBuffer buffer);

void initGraphics(volatile byte * framebuffer);

void setVoxel(int x, int y, int z);
void clearVoxel(int x, int y, int z);
void writeVoxel(int x, int y, int z, int value);
void line(int x1, int y1, int z1, int x2, int y2, int z2);

void setPlaneX(int x);
void clearPlaneX(int x);

void setPlaneY(int y);
void clearPlaneY(int y);

void setPlaneZ(int z);
void clearPlaneZ(int z);

void fill(byte pattern);


void copyCell(dpuCell origin, dpuCell destination);
void applyCellMap(dpuCellMap cellMap);


#ifdef __cplusplus
}
#endif
#endif // DPU_DRIVER_H