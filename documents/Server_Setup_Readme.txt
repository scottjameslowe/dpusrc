I. Install Ubuntu Server

	Note: if using pre-installed image, skip steps A and B

	A. Format SD card

		1. Download ubuntu server image (OMAP4)
			* http://cdimage.ubuntu.com/releases
			* this install used "ubuntu-12.04-preinstalled-server-armhf+omap4"
		2. Format card with image (DiskImager)


	B. SPECIAL NOTE: for "REV B3" boards, a bootloader patch is required.
		* procedure taken from: http://www.svtronics.com/support/pandaboard-es-b3-developers-guide/

		1. Download patches
			* git clone https://github.com/svtronics/u-boot-pandaboard-ES-RevB3.git
			* git clone https://github.com/svtronics/kernel-pandaboard-ES-RevB3.git
		2. Setup tool chain
			* sudo apt-get install build-essential gcc-arm-linux-gnueabi uboot-mkimage
		3. Build binaries
			* cd u-boot-pandaboard-ES-RevB3/
			* make CROSS_COMPILE=arm-linux-gnueabi- omap4_panda
		4. Replace these binaries on serv+er image:
			* u-boot.bin
			* MLO		


	C. Run Installation

		1. Insert SD card into Pandaboard
		2. Connect DSub serial port
		3. Open up serial connection
			* software: putty, teraterm, etc.
			* baud rate: 115200
			* data: 8 bit
			* parity: none
			* stop: 1bit
			* flow control: none
		4. Insert DC power jack, system should boot and immediately output on terminal
		5, Run through installation menu, using the following settings
			* Language [Engligh]
			* Location [United States]
			* Time zone [Eastern]
			* UTC [Yes]
			* Full name ["dpuadmin"]
			* User name ["dpuadmin"]
			* Password ["dpupass"]
			* Default network interface [wlan0]
				- Choose any working wireless essid/key, this install used essid:"PaulyShoreLivesHere" key:"s:ILoveLemonade"
			* Hostname ["dpuhost"]
			* Software [Basic Ubuntu server]
			
			
II. Setup Packages and Enviornment

	
		
		A. Install Node(v0.8.26)/NPM(1.2.30)
			1. Update and install dependencies
				* sudo apt-get update
				* sudo apt-get install git-core curl build-essential openssl libssl-dev python libcurl4-openssl-dev
			2. Download Node
				* wget http://nodejs.org/dist/v0.8.26/node-v0.8.26.tar.gz
			3. Unpack Node
				* tar -xvf node-v0.8.26.tar.gz
				* cd node-v0.8.26
			4. Edit configure files for Pandaboard
				* sudo nano deps/v8/build/common.gypi
					- add the following properties to the 'variables' object at the top of the document:
						'armv7%':'1',
						'arm_neon%':'1',
				* sudo nano deps/v8/SConstruct
					- make sure the code on line 83 & 84 matches this:
						'all': {
							'CCFLAGS':		['$DIALECTFLAGS', '$WARNINGFLAGS', '-march-armv7'],
							'CXXFLAGS':		['-fno-rtti', '-fno-exceptions', '-march-armv7'],
						},
			5. Configure, make, and install Node
				* ./configure --dest-cpu=arm
				* make
				* sudo make install
			6. Check versions to verify install
				* node -v
				* npm -v
				* response should read "v0.8.26" and "1.2.30"
		
		B. FTP Server
			1. Install ftp deamon
				* sudo apt-get install vsftpd
			2. Configure ftp settings
				* sudo nano /etc/vsftpd.conf
					- make sure each of the following is uncommented and matches:
						'anonymous_enable=NO'
						'local_enable=YES'
						'write_enable=YES'
						'ftpd_banner=Now connected to the dpu FTP service.'
			
			
		C. SSH Server
			1. Install ssh server
				* sudo apt-get install openssh-server
			
			
 ----- DPU BASELINE IMAGE -----
			
			
		D. dpu drivers
			* git clone http://dev.scottjameslowe.com/dpusrc.git -b dpu_server
			* cd dpusrc
			* ./install.sh
		
		E. (create a wireless ap)
		
		F. Reboot the system to make sure everything is installed and configured correctly
			* sudo reboot
			
			
		G. optimize performance at the expense of power consumption
			* "sudo sh -c "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor""
			
		H. disable "wait on ethernet" setting (by default, system waits 2 minutes if no ethernet connection is present)
			* edit /etc/network/interface, "nano /etc/network/interface"
				- change or add "eth0" so that it matches:
					"allow-hotplug eth0
						iface eth0 inet dhcp"
						
						
III. Setup DPU Server

		A. Setup source
			1. Download source
				* http://dev.scottjameslowe.com/dpusrc/get/server_deployment.tar.gz
			2. Unpackage source
				* tar -xvf server_deployment.tar.gz
						