Misc Pins

J3|1  - Bus 1v8 voltage reference
J3|2  - Bus 5v voltage reference
J3|27 - Bus Ground
J6|1  - Board Power 5v
J6|7  - Board Ground


Bus Pins


J6|17 (GPIO 36) - Bus Pin 1  (A1/Red)    - A6 (Data 7)
J6|18 (GPIO 32) - Bus Pin 2  (A2/Red)    - A7 (Data 8)
J6|19 (GPIO 61) - Bus Pin 3  (A3/Orange) - A4 (Data 5)
J6|20 (GPIO 33) - Bus Pin 4  (A4/Orange) - A5 (Data 6)
J6|21 (GPIO 54) - Bus Pin 5  (A5/Yellow) - A2 (Data 3)
J6|22 (GPIO 34) - Bus Pin 6  (A6/Yellow) - A3 (Data 4)
J6|23 (GPIO 55) - Bus Pin 7  (A7/Green)  - A0 (Data 1)
J6|24 (GPIO 35) - Bus Pin 8  (A8/Green)  - A1 (Data 2)
J6|25 (GPIO 50) - Bus Pin 9  (B1/Blue)   - B1 (Op 2)
J6|26 (GPIO 56) - Bus Pin 10 (B2/Blue)   - B0 (Op 1)
J6|27 (GPIO 51) - Bus Pin 11 (B3/Black)  - B3 (Op 3)
J6|28 (GPIO 59) - Bus Pin 12 (B4/Black)  - B2 (Op Trigger)


Data 1 (A0) - GPIO 55
Data 2 (A1) - GPIO 35
Data 3 (A2) - GPIO 54
Data 4 (A3) - GPIO 34
Data 5 (A4) - GPIO 61
Data 6 (A5) - GPIO 33
Data 7 (A6) - GPIO 36
Data 8 (A7) - GPIO 32
Op 1   (B0) - GPIO 56
Op 2   (B1) - GPIO 50
Op 3   (B3) - GPIO 51
Op Tgr (B2) - GPIO 59