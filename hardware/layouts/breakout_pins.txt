Breakout Pins
-------------

(PD1) Tx
(PD0) Rx

(PB7) SCK
(PB6) MISO
(PB5) MOSI
(PB4) SS

(PC1) SDA
(PC0) SCL

(PD2) INT0
(PD3) INT1

Analog 0
Analog 1
Analog 2
Analog 3
Analog 4
Analog 5
Analog 6
Analog 7